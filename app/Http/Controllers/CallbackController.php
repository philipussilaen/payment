<?php

namespace App\Http\Controllers;

use App\Http\Helper\Helper;
use App\Http\Libraries\BNIHashing;
use App\Http\Libraries\Doku\Doku_Initiate;
use App\Http\Libraries\Doku\Doku_Library;
use App\Http\Libraries\MolPay;
use App\Http\Libraries\MidtransAPI;
use App\Http\Models\apiV1\BNIVirtualAccount;
use App\Http\Models\apiV1\BNIVirtualAccountHistory;
use App\Http\Models\apiV1\ClientTransaction;
use App\Http\Models\apiV1\ClientTransactionDetail;
use App\Http\Models\apiV1\ClientTransactionHistory;
use App\Http\Models\apiV1\DokuTransaction;
use App\Http\Models\apiV1\MolPayTransaction;
use App\Http\Models\apiV1\PaymentChannel;
use App\Http\Models\apiV1\PaymentCharge;
use App\Http\Models\apiV1\OttopayTransactions;
use App\Http\Models\apiV1\TcashTransactions;
use App\Http\Models\apiV1\MidtransTransactions;
use App\Jobs\SendClientCallback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CallbackController extends Controller
{
    /**
     * BNI Callback
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function BniCallback(Request $request){
        $input = $request->input();
        Helper::LogPayment("Begin BNI Callback. ".json_encode($input),'bni','callback');
        if (is_string($input)) $inputJson = json_decode($input,true);
        else $inputJson = $input;
        $status = '999';
        $errorMsg = null;
        $apiBNI = new \App\Http\Libraries\BNIVirtualAccount();

        $clientId = $apiBNI->clientId;
        $secretKey = $apiBNI->secretKey;

        if (!$inputJson){
            $errorMsg = 'Unauthorized';
            $response = ['status'=> $status,'message'=>$errorMsg];
            Helper::LogPayment("Response ".json_encode($response),'bni','callback');
            return response()->json($response);
        }

        if ($inputJson['client_id'] !== $clientId){
            Helper::LogPayment("Invalid Client Id ",'bni','callback');
            return;
        }

        $inputParsed = BNIHashing::parseData($inputJson['data'],$clientId,$secretKey);
        if (!$inputParsed){
            $errorMsg = 'Invalid time or key';
            $response = ['status'=> $status,'message'=>$errorMsg];
            Helper::LogPayment("Response ".json_encode($response),'bni','callback');
            return response()->json($response);
        }

        $paymentId = $inputParsed['trx_id'];
        $virtualAccount = $inputParsed['virtual_account'];
        $transactionAmount = $inputParsed['trx_amount'];
        $lastPaymentAmount = $inputParsed['payment_amount'];
        $cumulativePaymentAmount = $inputParsed['cumulative_payment_amount'];
        $paymentNtb = $inputParsed['payment_ntb'];
        $datetimePayment= $inputParsed['datetime_payment'];
        Helper::LogPayment("Input Parsed ".json_encode($inputParsed),'bni','callback');

        // find Payment Id on transaction
        $clientTransactionDb = ClientTransaction::where('payment_id',$paymentId)->first();
        if (!$clientTransactionDb){
            $errorMsg = 'Invalid Transaction/Payment Id';
            $response = ['status'=> $status,'message'=>$errorMsg];
            Helper::LogPayment("Response ".json_encode($response),'bni','callback');
            return response()->json($response);
        }
        $clientTransactionId = $clientTransactionDb->id;
        $BNIVirtualDb = BNIVirtualAccount::where('client_transactions_id',$clientTransactionId)->first();
        if (!$BNIVirtualDb){
            $errorMsg = 'Invalid Transaction/Payment Id';
            $response = ['status'=> $status,'message'=>$errorMsg];
            Helper::LogPayment("Response ".json_encode($response),'bni','callback');
            return response()->json($response);
        }
        $BNIVirtualId = $BNIVirtualDb->id;

        // if Transaction Fixed and already PAID
        if ($clientTransactionDb->payment_type != 'open' && $clientTransactionDb->status == 'PAID'){
            $status = '000';
            $response = ['status'=> $status];
            return response()->json($response);
        }

        Helper::LogPayment("Begin Update DB",'bni','callback');
        DB::beginTransaction();

        // update BNI Virtual Account
        $BNIVirtualDb = BNIVirtualAccount::find($BNIVirtualId);
        $BNIVirtualDb->datetime_payment = date('Y-m-d H:i:s',strtotime($datetimePayment));
        $BNIVirtualDb->payment_ntb = $paymentNtb;
        $BNIVirtualDb->payment_amount = $cumulativePaymentAmount;
        $BNIVirtualDb->last_payment_amount = $lastPaymentAmount;
        $BNIVirtualDb->save();
        Helper::LogPayment("Update BNI Virtual Account datetime payment $datetimePayment payment Ntb $paymentNtb",'bni','callback');

        // create history
        $paramHistory=[];
        $paramHistory['amount'] = $transactionAmount;
        $paramHistory['customerName'] = $clientTransactionDb->customer_name;
        $paramHistory['datetimeExpired'] = $BNIVirtualDb->datetime_expired;
        $paramHistory['description'] = 'BNI PAID';
        $paramHistory['paymentAmount'] = $lastPaymentAmount;
        $paramHistory['cumulativeAmount'] = $cumulativePaymentAmount;
        $paramHistory['paymentNtb'] = $paymentNtb;
        $paramHistory['dateTimePayment'] = date('Y-m-d H:i:s',strtotime($datetimePayment));
        BNIVirtualAccountHistory::createHistory($BNIVirtualId,$paramHistory);
        Helper::LogPayment("Insert History BNI ".json_encode($paramHistory),'bni','callback');

        // update transactionDB
        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        $clientTransactionDb->status = 'PAID';
        $clientTransactionDb->total_payment = $cumulativePaymentAmount;
        $clientTransactionDb->last_payment = $lastPaymentAmount;
        $clientTransactionDb->save();
        Helper::LogPayment("Update Transaction $clientTransactionId to PAID",'bni','callback');

        // insert history
        ClientTransactionHistory::insertHistory($clientTransactionId,'PAID','PAID',$lastPaymentAmount);
        Helper::LogPayment("Insert History Transaction PAID",'bni','callback');

        // if payment type OPEN
        if ($clientTransactionDb->payment_type == 'open'){
            $paymentChargesDb = PaymentCharge::where('payment_channels_id',$clientTransactionDb->payment_channels_id)
                ->where('payment_type',$clientTransactionDb->payment_type)
                ->where('charges_type','subtract')
                ->get();
            $paymentChannelDb = PaymentChannel::find($clientTransactionDb->payment_channels_id);

            $channelAdminFee =0;
            $customerAdminFee = 0;
            if ($paymentChannelDb){
                $channelAdminFee = $paymentChannelDb->admin_fee;
            }

            foreach ($paymentChargesDb as $item) {
                $chargeType = $item->type;
                $chargeValue = $item->value;
                $chargeName = $item->name;
                $chargeAmount = 0;
                if ($chargeType == 'fixed') $chargeAmount = $chargeValue;
                elseif ($chargeType == 'percent') {
                    $chargeAmount = $lastPaymentAmount * $chargeValue / 100;
                }
                if ($lastPaymentAmount < 100000){
                    $customerAdminFee = $chargeAmount;
                }
            }

            // insert new detail
            $tmp = new \stdClass();
            $tmp->description = 'PAID Open Bill from '.$clientTransactionDb->payment_id;
            $randomString = Helper::generateRandomString(3);
            $nowDateTime = date('ymdhis');
            $subPaymentId = $clientTransactionDb->payment_channel_code."DTL$nowDateTime$randomString";
            $tmp->sub_payment_id = $subPaymentId;
            $tmp->amount = $lastPaymentAmount - $customerAdminFee;
            $tmp->paid_amount = $lastPaymentAmount;
            $tmp->status = 'PAID';
            $tmp->channel_admin_fee = $channelAdminFee;
            $tmp->customer_admin_fee = $customerAdminFee;
            $transactionItems[] = $tmp;

            Helper::LogPayment("Transaction Items : ".json_encode($transactionItems));
            ClientTransactionDetail::insertTransactionDetail($clientTransactionId,$transactionItems);
        }

        BNIVirtualAccount::inquiryVA($paymentId);

        $status = '000';
        $response = ['status'=> $status];
        DB::commit();
        Helper::LogPayment("End Update DB & callback ".json_encode($response),'bni','callback');

        Helper::LogPayment("Dispatch Client Callback",'bni','callback');
        dispatch(new SendClientCallback($clientTransactionDb));

        return response()->json($response);
    }

    /**
     * MOLPay Return
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function molPayReturn(Request $request){
        $input = $request->input();
        Helper::LogPayment("Begin MolPay Return Callback. ".json_encode($input),'molpay','callback');

        $tranId = $request->input('tranID');
        $orderId = $request->input('orderid');
        $status = $request->input('status');
        $domain = $request->input('domain');
        $amount = $request->input('amount');
        $currency = $request->input('currency');
        $appCode = $request->input('appcode');
        $payDate = $request->input('paydate');
        $sKey = $request->input('skey');

        $molPay = new MolPay();
        $vKey = $molPay->verificationKey;

        $key0 = md5($tranId.$orderId.$status.$domain.$amount.$currency);
        $key1 = md5($payDate.$domain.$key0.$appCode.$vKey);

        Helper::LogPayment("Verify $sKey - $key1",'molpay','callback');

        if ($sKey != $key1) {
            $status = -1;
            Helper::LogPayment("Invalid Transaction sKey and key1",'molpay','callback');
            return view('callback.molpay-return');
        }

        if ($status == "00"){
            Helper::LogPayment("Transaction status Success 00",'molpay','callback');
        } else {
            Helper::LogPayment("Transaction status $status",'molpay','callback');
        }

        // find Payment Id on transaction
        $clientTransactionDb = ClientTransaction::where('payment_id',$orderId)->first();
        if (!$clientTransactionDb){
            $errorMsg = 'Invalid Transaction/Payment Id';
            $response = ['status'=> $status,'message'=>$errorMsg];
            Helper::LogPayment("Response ".json_encode($response),'bni','callback');
            return view('callback.molpay-return');
        }
        if ($clientTransactionDb->status == 'PAID'){
            $status = '000';
            $response = ['status'=> $status];
            Helper::LogPayment("Insert History Transaction Already PAID",'molpay','callback');
            return;
        }
        $clientTransactionId = $clientTransactionDb->id;
        $molPayTransactionDb = MolPayTransaction::where('client_transactions_id',$clientTransactionId)->first();
        if (!$molPayTransactionDb){
            $errorMsg = 'Invalid Transaction/Payment Id';
            $response = ['status'=> $status,'message'=>$errorMsg];
            Helper::LogPayment("Response ".json_encode($response),'bni','callback');
            return view('callback.molpay-return');
        }
        $molPayTransId = $molPayTransactionDb->id;
        Helper::LogPayment("Begin Update DB",'molpay','callback');

        $paymentStatus = 'PENDING';
        if ($status == "00") $paymentStatus = 'PAID';
        if ($status == "11") $paymentStatus = 'FAILED';
        if ($status == "22") $paymentStatus = 'PENDING';
        $viewStatus = 'FAILED';

        DB::beginTransaction();

        // update molpay transaction
        $molPayTransactionDb = MolPayTransaction::find($molPayTransId);
        $molPayTransactionDb->transaction_id = $tranId;
        $molPayTransactionDb->domain = $domain;
        $molPayTransactionDb->app_code = $appCode;
        $molPayTransactionDb->pay_date = $payDate;
        $molPayTransactionDb->s_key = $sKey;
        $molPayTransactionDb->callback = json_encode($input);
        $molPayTransactionDb->save();
        Helper::LogPayment("Update MolPay Transaction tranId $tranId domain $domain appcode $appCode paydate $payDate skey $sKey",'molpay','callback');

        // update transactionDB
        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        $clientTransactionDb->status = $paymentStatus;
        $clientTransactionDb->total_payment = $amount;
        $clientTransactionDb->last_payment = $amount;
        $clientTransactionDb->save();
        Helper::LogPayment("Update Transaction $clientTransactionId to $paymentStatus",'molpay','callback');

        // insert history
        ClientTransactionHistory::insertHistory($clientTransactionId,$paymentStatus,$paymentStatus,$amount);
        Helper::LogPayment("Insert History Transaction $paymentStatus",'molpay','callback');

        $status = '000';
        $response = ['status'=> $status];
        DB::commit();

        Helper::LogPayment("End Update DB & callback ".json_encode($response),'molpay','callback');

        if ($paymentStatus == 'PAID'){
            $viewStatus = 'SUCCESS';
            Helper::LogPayment("Dispatch Client Callback",'molpay','callback');
            dispatch(new SendClientCallback($clientTransactionDb));
        } else {
            Helper::LogPayment("NOT Dispatch Client $paymentStatus",'molpay','callback');
        }
        $data = [];
        $data['status'] = $viewStatus;
        return view('callback.molpay-return',$data);
    }

    /**
     * MolPay Notify with IPN
     * @param Request $request
     */
    public function molPayNotify(Request $request){
        $input = $request->input();
        Helper::LogPayment("Begin MolPay Notify Callback. ".json_encode($input),'molpay','callback');

        $tranId = $request->input('tranID');
        $orderId = $request->input('orderid');
        $status = $request->input('status');
        $domain = $request->input('domain');
        $amount = $request->input('amount');
        $currency = $request->input('currency');
        $appCode = $request->input('appcode');
        $payDate = $request->input('paydate');
        $sKey = $request->input('skey');

        $input['treq'] = 1; #Additional Parameter for IPN. Value Always set to 1

        /*IPN Process*/
        $postData = [];
        foreach ($input as $key => $value) {
            $postData[] = "$key=$value";
        }
        $this->molPayIPN($postData);

        $molPay = new MolPay();
        $vKey = $molPay->verificationKey;

        $key0 = md5($tranId.$orderId.$status.$domain.$amount.$currency);
        $key1 = md5($payDate.$domain.$key0.$appCode.$vKey);

        Helper::LogPayment("Verify $sKey - $key1",'molpay','callback');

        if ($sKey != $key1) {
            $status = -1;
            Helper::LogPayment("Invalid Transaction sKey and key1",'molpay','callback');
            return;
        }

        if ($status == "00"){
            Helper::LogPayment("Transaction status Success 00",'molpay','callback');
        } else {
            Helper::LogPayment("Transaction status $status",'molpay','callback');
        }

        // find Payment Id on transaction
        $clientTransactionDb = ClientTransaction::where('payment_id',$orderId)->first();
        if (!$clientTransactionDb){
            $errorMsg = 'Invalid Transaction/Payment Id';
            $response = ['status'=> $status,'message'=>$errorMsg];
            Helper::LogPayment("Response ".json_encode($response),'bni','callback');
            return;
        }
        if ($clientTransactionDb->status == 'PAID'){
            $status = '000';
            $response = ['status'=> $status];
            Helper::LogPayment("Insert History Transaction Already PAID",'molpay','callback');
            return;
        }
        $clientTransactionId = $clientTransactionDb->id;
        $molPayTransactionDb = MolPayTransaction::where('client_transactions_id',$clientTransactionId)->first();
        if (!$molPayTransactionDb){
            $errorMsg = 'Invalid Transaction/Payment Id';
            $response = ['status'=> $status,'message'=>$errorMsg];
            Helper::LogPayment("Response ".json_encode($response),'bni','callback');
            return;
        }
        $molPayTransId = $molPayTransactionDb->id;
        Helper::LogPayment("Begin Update DB",'molpay','callback');

        $paymentStatus = 'PENDING';
        if ($status == "00") $paymentStatus = 'PAID';
        if ($status == "11") $paymentStatus = 'FAILED';
        if ($status == "22") $paymentStatus = 'PENDING';

        DB::beginTransaction();

        // update molpay transaction
        $molPayTransactionDb = MolPayTransaction::find($molPayTransId);
        $molPayTransactionDb->transaction_id = $tranId;
        $molPayTransactionDb->domain = $domain;
        $molPayTransactionDb->app_code = $appCode;
        $molPayTransactionDb->pay_date = $payDate;
        $molPayTransactionDb->s_key = $sKey;
        $molPayTransactionDb->callback = json_encode($input);
        $molPayTransactionDb->save();
        Helper::LogPayment("Update MolPay Transaction tranId $tranId domain $domain appcode $appCode paydate $payDate skey $sKey",'molpay','callback');

        // update transactionDB
        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        $clientTransactionDb->status = $paymentStatus;
        $clientTransactionDb->total_payment = $amount;
        $clientTransactionDb->last_payment = $amount;
        $clientTransactionDb->save();
        Helper::LogPayment("Update Transaction $clientTransactionId to $paymentStatus",'molpay','callback');

        // insert history
        ClientTransactionHistory::insertHistory($clientTransactionId,$paymentStatus,$paymentStatus,$amount);
        Helper::LogPayment("Insert History Transaction $paymentStatus",'molpay','callback');

        $status = '000';
        $response = ['status'=> $status];
        DB::commit();

        Helper::LogPayment("End Update DB & callback ".json_encode($response),'molpay','callback');

        if ($paymentStatus == 'PAID'){
            Helper::LogPayment("Dispatch Client Callback",'molpay','callback');
            dispatch(new SendClientCallback($clientTransactionDb));
        } else {
            Helper::LogPayment("NOT Dispatch Client $paymentStatus",'molpay','callback');
        }
        return;
    }

    /**
     * Molpay Callback
     * @param Request $request
     */
    public function molPayCallback(Request $request){
        $input = $request->input();
        Helper::LogPayment("Begin MolPay Callback Callback. ".json_encode($input),'molpay','callback');

        $nbcb = $request->input('nbcb');
        $tranId = $request->input('tranID');
        $orderId = $request->input('orderid');
        $status = $request->input('status');
        $domain = $request->input('domain');
        $amount = $request->input('amount');
        $currency = $request->input('currency');
        $appCode = $request->input('appcode');
        $payDate = $request->input('paydate');
        $sKey = $request->input('skey');

        $molPay = new MolPay();
        $vKey = $molPay->verificationKey;

        $key0 = md5($tranId.$orderId.$status.$domain.$amount.$currency);
        $key1 = md5($payDate.$domain.$key0.$appCode.$vKey);

        Helper::LogPayment("Verify $sKey - $key1",'molpay','callback');

        if ($sKey != $key1) {
            $status = -1;
            Helper::LogPayment("Invalid Transaction sKey and key1",'molpay','callback');
            return;
        }

        if ($status == "00"){
            Helper::LogPayment("Transaction status Success 00",'molpay','callback');
        } else {
            Helper::LogPayment("Transaction status $status",'molpay','callback');
        }

        // find Payment Id on transaction
        $clientTransactionDb = ClientTransaction::where('payment_id',$orderId)->first();
        if (!$clientTransactionDb){
            $errorMsg = 'Invalid Transaction/Payment Id';
            $response = ['status'=> $status,'message'=>$errorMsg];
            Helper::LogPayment("Response ".json_encode($response),'bni','callback');
            return;
        }
        if ($clientTransactionDb->status == 'PAID'){
            if ( $nbcb==1 ) {
                Helper::LogPayment("CBTOKEN:MPSTATOK",'molpay','callback');
                //callback IPN feedback to notified MOLPay
                echo "CBTOKEN:MPSTATOK"; exit;
            }
            $status = '000';
            $response = ['status'=> $status];
            Helper::LogPayment("Insert History Transaction Already PAID",'molpay','callback');
            return;
        }
        $clientTransactionId = $clientTransactionDb->id;
        $molPayTransactionDb = MolPayTransaction::where('client_transactions_id',$clientTransactionId)->first();
        if (!$molPayTransactionDb){
            $errorMsg = 'Invalid Transaction/Payment Id';
            $response = ['status'=> $status,'message'=>$errorMsg];
            Helper::LogPayment("Response ".json_encode($response),'bni','callback');
            return;
        }
        $molPayTransId = $molPayTransactionDb->id;
        Helper::LogPayment("Begin Update DB",'molpay','callback');

        $paymentStatus = 'PENDING';
        if ($status == "00") $paymentStatus = 'PAID';
        if ($status == "11") $paymentStatus = 'FAILED';
        if ($status == "22") $paymentStatus = 'PENDING';

        DB::beginTransaction();

        // update molpay transaction
        $molPayTransactionDb = MolPayTransaction::find($molPayTransId);
        $molPayTransactionDb->transaction_id = $tranId;
        $molPayTransactionDb->domain = $domain;
        $molPayTransactionDb->app_code = $appCode;
        $molPayTransactionDb->pay_date = $payDate;
        $molPayTransactionDb->s_key = $sKey;
        $molPayTransactionDb->callback = json_encode($input);
        $molPayTransactionDb->save();
        Helper::LogPayment("Update MolPay Transaction tranId $tranId domain $domain appcode $appCode paydate $payDate skey $sKey",'molpay','callback');

        // update transactionDB
        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        $clientTransactionDb->status = $paymentStatus;
        $clientTransactionDb->total_payment = $amount;
        $clientTransactionDb->last_payment = $amount;
        $clientTransactionDb->save();
        Helper::LogPayment("Update Transaction $clientTransactionId to $paymentStatus",'molpay','callback');

        // insert history
        ClientTransactionHistory::insertHistory($clientTransactionId,$paymentStatus,$paymentStatus,$amount);
        Helper::LogPayment("Insert History Transaction $paymentStatus",'molpay','callback');

        $status = '000';
        $response = ['status'=> $status];
        DB::commit();

        Helper::LogPayment("End Update DB & callback ".json_encode($response),'molpay','callback');

        if ($paymentStatus == 'PAID'){
            Helper::LogPayment("Dispatch Client Callback",'molpay','callback');
            dispatch(new SendClientCallback($clientTransactionDb));
        } else {
            Helper::LogPayment("NOT Dispatch Client $paymentStatus",'molpay','callback');
        }
        if ( $nbcb==1 ) {
            Helper::LogPayment("CBTOKEN:MPSTATOK",'molpay','callback');
            //callback IPN feedback to notified MOLPay
            echo "CBTOKEN:MPSTATOK"; exit;
        }
        Helper::LogPayment("return",'molpay','callback');
        return;
    }

    /**
     * Doku Notify
     * @param Request $request
     */
    public function dokuNotify(Request $request){
        $echo = 'STOP';
        $input = $request->input();
        Helper::LogPayment("Begin Doku Callback. ".json_encode($input),'doku','callback');

        $paymentId = $request->input('TRANSIDMERCHANT');
        $bank = $request->input('BANK');
        $responseCode = $request->input('RESPONSECODE');
        $paymentDatetime = $request->input('PAYMENTDATETIME');
        $paymentChannel = $request->input('PAYMENTCHANNEL');
        $newPaymentAmount = $request->input("AMOUNT");
        $mcn = $request->input('MCN');
        $resultMessage = $request->input('RESULTMSG');
        $statusType = $request->input('STATUSTYPE');
        $approvalCode = $request->input('APPROVALCODE');
        $brand = $request->input('BRAND');
        $verifyStatus = $request->input('VERIFYSTATUS');
        $requestWords = $request->input('WORDS');

        // generated word
        $params = [];
        $params['amount'] = $newPaymentAmount;
        $params['invoice'] = $paymentId;

        // check transaction exist
        $clientTransactionDb = ClientTransaction::where('payment_id',$paymentId)->first();
        if (!$clientTransactionDb){
            echo "TransID Merchant not valid";
            exit();
        }
        $dokuChannelCode = $clientTransactionDb->payment_channel_code;

        $dokuInitiate = new Doku_Initiate();
        $mallId = $dokuInitiate->mallId;
        $sharedKey = $dokuInitiate->sharedKey;

        if ($dokuChannelCode == 'DOKU-BCA'){
            if ($clientTransactionDb->payment_type == 'open'){
                $mallId = $dokuInitiate->bcaOpenMallId;
                $sharedKey = $dokuInitiate->bcaOpenSharedKey;
            } else {
                $mallId = $dokuInitiate->bcaCloseMallId;
                $sharedKey = $dokuInitiate->bcaCloseSharedKey;
            }
        }

        Helper::LogPayment("$newPaymentAmount.$mallId.$sharedKey.$paymentId.$resultMessage.$verifyStatus",'doku','callback');

        $words = sha1($newPaymentAmount.$mallId.$sharedKey.$paymentId.$resultMessage.$verifyStatus);

        Helper::LogPayment("Request Word $requestWords | Words Generated $words",'doku','callback');

        if ($words == $requestWords){
            Helper::LogPayment("Payment Status $resultMessage",'doku','callback');

            $clientTransactionId = $clientTransactionDb->id;
            $totalPaymentAmount = $clientTransactionDb->total_payment;

            // check if closed and paid
            if($clientTransactionDb->payment_type != 'open' && $clientTransactionDb->status == 'PAID'){
                echo "CONTINUE";
                exit();
            }

            $dokuTransactionDb = DokuTransaction::where('client_transactions_id',$clientTransactionId)->first();
            if (!$dokuTransactionDb){
                echo "Doku Transaction not valid";
                exit();
            }
            $dokuTransactionId = $dokuTransactionDb->id;

            $echo = 'CONTINUE';
            $paymentStatus = 'FAILED';
            if ($resultMessage == 'SUCCESS') $paymentStatus = 'PAID';
            if (!empty($paymentDatetime)) $paymentDatetime = date('Y-m-d H:i:s',strtotime($paymentDatetime));

            Helper::LogPayment("Begin Update DB",'doku','callback');
            DB::beginTransaction();
            $totalPaymentAmount = $totalPaymentAmount + $newPaymentAmount;

            $dokuTransactionDb = DokuTransaction::find($dokuTransactionId);
            $dokuTransactionDb->payment_channel = $paymentChannel;
            $dokuTransactionDb->mcn = $mcn;
            $dokuTransactionDb->words = $requestWords;
            $dokuTransactionDb->result = $resultMessage;
            $dokuTransactionDb->bank = $bank;
            $dokuTransactionDb->status_type = $statusType;
            $dokuTransactionDb->approval_code = $approvalCode;
            $dokuTransactionDb->response_code = $responseCode;
            $dokuTransactionDb->brand = $brand;
            $dokuTransactionDb->payment_date = $paymentDatetime;
            $dokuTransactionDb->payment_amount = $totalPaymentAmount;
            $dokuTransactionDb->last_payment_amount = $newPaymentAmount;
            $dokuTransactionDb->save();

            // update transactionDB
            $clientTransactionDb = ClientTransaction::find($clientTransactionId);
            $clientTransactionDb->status = $paymentStatus;
            $clientTransactionDb->total_payment = $totalPaymentAmount;
            $clientTransactionDb->last_payment = $newPaymentAmount;
            $clientTransactionDb->save();
            Helper::LogPayment("Update Transaction $clientTransactionId to $paymentStatus",'doku','callback');

            // insert history
            ClientTransactionHistory::insertHistory($clientTransactionId,$paymentStatus,$paymentStatus,$newPaymentAmount);
            Helper::LogPayment("Insert History Transaction $paymentStatus",'doku','callback');

            // if payment type OPEN
            if ($clientTransactionDb->payment_type == 'open'){
                $paymentChargesDb = PaymentCharge::where('payment_channels_id',$clientTransactionDb->payment_channels_id)
                    ->where('payment_type',$clientTransactionDb->payment_type)
                    ->where('charges_type','subtract')
                    ->get();
                $paymentChannelDb = PaymentChannel::find($clientTransactionDb->payment_channels_id);
                $channelAdminFee =0;
                $customerAdminFee = 0;
                if ($paymentChannelDb){
                    $channelAdminFee = $paymentChannelDb->admin_fee;
                }

                foreach ($paymentChargesDb as $item) {
                    $chargeType = $item->type;
                    $chargeValue = $item->value;
                    $chargeName = $item->name;
                    $chargeAmount = 0;
                    if ($chargeType == 'fixed') $chargeAmount = $chargeValue;
                    elseif ($chargeType == 'percent') {
                        $chargeAmount = $newPaymentAmount * $chargeValue / 100;
                    }
                    if ($newPaymentAmount < 100000){
                        $customerAdminFee = $chargeAmount;
                    }
                }

                // insert new detail
                $tmp = new \stdClass();
                $tmp->description = 'PAID Open Bill from '.$clientTransactionDb->payment_id;
                $randomString = Helper::generateRandomString(3);
                $nowDateTime = date('ymdhis');
                $subPaymentId = $clientTransactionDb->payment_channel_code."DTL$nowDateTime$randomString";
                $tmp->sub_payment_id = $subPaymentId;
                $tmp->amount = $newPaymentAmount - $customerAdminFee;
                $tmp->paid_amount = $newPaymentAmount;
                $tmp->status = 'PAID';
                $tmp->channel_admin_fee = $channelAdminFee;
                $tmp->customer_admin_fee = $customerAdminFee;
                $transactionItems[] = $tmp;

                Helper::LogPayment("Transaction Items : ".json_encode($transactionItems));
                ClientTransactionDetail::insertTransactionDetail($clientTransactionId,$transactionItems);
            } else {
                // update client transaction detail to paid
                $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
                if ($clientTransactionDetailDb){
                    $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
                    $clientTransactionDetailDb->paid_amount = $newPaymentAmount;
                    $clientTransactionDetailDb->status = 'PAID';
                    $clientTransactionDetailDb->save();
                }
            }

            DB::commit();

            Helper::LogPayment("End Update DB & callback ",'doku','callback');

            if ($paymentStatus == 'PAID'){
                Helper::LogPayment("Dispatch Client Callback",'doku','callback');
                dispatch(new SendClientCallback($clientTransactionDb));
            } else {
                Helper::LogPayment("NOT Dispatch Client $paymentStatus",'doku','callback');
            }
            echo $echo;
            exit();
        } else {
            Helper::LogPayment("Word Not Match",'doku','callback');
            $echo = 'WORDS NOT MATCH';
        }
        echo $echo;
    }

    /**
     * Doku Redirect
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dokuRedirect(Request $request){
        $input = $request->input();
        Helper::LogPayment("Begin Doku Redirect. ".json_encode($input),'doku','callback');

        $paymentId = $request->input('TRANSIDMERCHANT');

        // default view
        $data = [];
        $data['status'] = 'failed';
        $data['payChannel'] = 'DOKU';

        // get client Transaction DB
        $clientTransactionDb = ClientTransaction::where('payment_id',$paymentId)->first();
        if (!$clientTransactionDb) {
            Helper::LogPayment('Invalid Payment Id/Not Found','doku','callback');
            return view('callback.landing',$data);
        }

        $statusPayment = $clientTransactionDb->status;
        if ($statusPayment=='PAID') $data['status'] = 'success';

        return view('callback.landing',$data);
    }

    /**
     * Doku Open ATM Inquiry
     * @param Request $request
     * @return mixed
     */
    public function dokuOpenNotify(Request $request){
        Helper::LogPayment("Begin Doku ATM Open Inquiry ".json_encode($request->input()),'doku','inquiry');
        $paymentChannel = $request->input('PAYMENTCHANNEL');
        $vaNumber = $request->input('PAYMENTCODE');
        $requestWords = $request->input('WORDS');

        // search virtual number
        $dokuTransactionDb = DokuTransaction::where('payment_code',$vaNumber)->orderBy('created_at','DESC')->first();
        if (!$dokuTransactionDb){
            Helper::LogPayment("Doku VA not Found",'doku','inquiry');
            echo "9999 : Payment Code not exist";
            exit();
        }
        $clientTransactionId = $dokuTransactionDb->client_transactions_id;

        $dokuInitiate = new Doku_Initiate();
        $mallId = $dokuInitiate->bcaOpenMallId;
        $sharedKey = $dokuInitiate->bcaOpenMallId;

        $amount = $dokuTransactionDb->transaction_amount;
        $billingType = $dokuTransactionDb->billing_type;
        $dokuChannelCode = $dokuTransactionDb->method_code;

        if ($billingType=='open') {
            $amount = '0.00';
        }
        if ($dokuChannelCode == 'DOKU-BCA'){
            $mallId = $dokuInitiate->bcaOpenMallId;
            $sharedKey = $dokuInitiate->bcaOpenSharedKey;
        }

        $words = sha1($mallId.$sharedKey.$vaNumber);

        Helper::LogPayment("Request Word $requestWords | Words Generated $mallId.$sharedKey.$vaNumber = $words",'doku','inquiry');

        if ($words == $requestWords){
            // check transaction exist
            $clientTransactionDb = ClientTransaction::find($clientTransactionId);
            if (!$clientTransactionDb){
                Helper::LogPayment("Trans ID merchant not valid",'doku','inquiry');
                echo "9999 : Payment Code not exist";
                exit(9999);
            }
            $dokuTransactionId = $dokuTransactionDb->id;
            $paymentId = $clientTransactionDb->payment_id;

            Helper::LogPayment("Begin Update DB",'doku','inquiry');
            DB::beginTransaction();

            // create sub detail item
            /*$tmp = new \stdClass();
            $tmp->description = 'Open Bill from '.$clientTransactionDb->payment_id;
            $randomString = Helper::generateRandomString(3);
            $nowDateTime = date('ymdhis');
            $subPaymentId = $clientTransactionDb->payment_channel_code."DTL$nowDateTime$randomString";
            $tmp->sub_payment_id = $subPaymentId;
            $tmp->amount = 0;
            $tmp->status = 'CREATED';
            $transactionItems[] = $tmp;
            Helper::LogPayment("Transaction Items : ".json_encode($transactionItems));
            ClientTransactionDetail::insertTransactionDetail($clientTransactionId,$transactionItems);*/

            // create parameter for xml view
            $email = $clientTransactionDb->customer_email;
            if (empty($email)) $email = 'it@popbox.asia';
            $description = $clientTransactionDb->description;

            if (empty($description)) $description = $paymentId;
            $tmpDescription = str_replace(',','',$description);
            $basket = "$tmpDescription,$amount,1,$amount";

            $generatedWord = sha1($amount.$mallId.$sharedKey.$paymentId);
            $xml = [];
            $xml['PAYMENTCODE'] = $vaNumber;
            $xml['AMOUNT'] = $amount;
            $xml['PURCHASEAMOUNT'] = $amount;
            $xml['TRANSIDMERCHANT'] = $paymentId;
            $xml['WORDS'] = $generatedWord;
            $xml['REQUESTDATETIME'] = date('YmdHis');
            $xml['CURRENCY'] = '360';
            $xml['PURCHASECURRENCY'] = '360';
            $xml['SESSIONID'] = uniqid();
            $xml['NAME'] = $clientTransactionDb->customer_name;
            $xml['EMAIL'] = $email;
            $xml['BASKET'] = $basket;
            $xml['ADDITIONALDATA'] = $description;
            DB::commit();

            Helper::LogPayment("End ATM Inquiry response XML ".json_encode($xml),'doku','inquiry');
            $data = [];
            $data['params'] = $xml;
            return response()->view('doku-xml',$data)->header('Content-Type','text/xml');
        } else {
            Helper::LogPayment("Word Not Match",'doku','inquiry');
            $echo = 'WORDS NOT MATCH';
        }
        echo $echo;
        exit();
    }

    public function ottoPayNotify(Request $request)
    {
        Helper::LogPayment("Callback Payment From Ottopay!");
        $response = new \stdClass();
        $response->responseCode = "401";
        $response->responseDescription = "Failed";

        $paymentId = $request->input('orderId',null);
        $clientTransactionDb = ClientTransaction::with('OttoPay')
            ->where('payment_id',$paymentId)
            ->first();
        $client = $clientTransactionDb->OttoPay->client;

        if (isset($clientTransactionDb->status) && $clientTransactionDb->status == "PAID") {
            $response->responseCode = "200";
            $response->responseDescription = "Success";
            Helper::LogPayment("Transactions Already Paid -> ". $request->getContent());
            return response()->json($response);
        } else if ($clientTransactionDb->status == "CREATED") {

            if (!($clientTransactionDb)){
                Helper::LogPayment("Payment Status Ottopay -> ". $response->responseDescription ." -> ". $request->getContent());
                return response()->json($response);
            }
    
            $ottoPayDb = $clientTransactionDb->OttoPay;
            if (!($ottoPayDb) || ($ottoPayDb->status == 'Expired')){
                Helper::LogPayment("Payment Status Ottopay -> ". $response->responseDescription ." -> ". $request->getContent());
                return response()->json($response);            

            }

            // inquiry with
            $ottoPay = OttopayTransactions::inquiryOttoPayment($request, $paymentId, $client);
        } else {
            Helper::LogPayment("Payment Status Ottopay -> ". $response->responseDescription ." -> ". $request->getContent());
            return response()->json($response);            
        }

        
        $response->responseCode = "200";
        $response->responseDescription = "Success";

        // Send Client Callback to Agent
        dispatch(new SendClientCallback($clientTransactionDb)); 

        Helper::LogPayment("End Callback Payment Notifications From Ottopay -> ". $response->responseDescription ." -> ". $request->getContent());
        return response()->json($response);
    }

    public function tcashBillPayment(Request $request)
    {
        Helper::LogPayment("Callback Payment Notifications!");
        $response = "01:Transaction Not Found";
        $tcashRes  = null;

        $paymentId = $request->input('acc_no',null);
        $inquiryPayment = $request->input('trx_type');

        $clientTransactionDb = ClientTransaction::with('tCash')
            ->where('payment_id',$paymentId)
            ->first();

        if (isset($clientTransactionDb->status) && $clientTransactionDb->status == "PAID") {
            $tcashRes = $clientTransactionDb->tCash->trx_id.":".$paymentId.":Sudah Dibayar";
        } else {
            if (!$clientTransactionDb){
                return $response;
            }
    
            $tcashDb = $clientTransactionDb->tCash;
            if (!$tcashDb){
                return $response;            
            }
            
            if ($inquiryPayment == '021') {
                $tCash = TcashTransactions::inquiry($request, $paymentId);
                
                if ($tCash->isSuccess) {
                    $tcashRes = "POPBOX:".$tCash->data['bill_amount'].":".$tCash->data['bill_ref'].":Inquiry Success";
                } else {
                    return $response;
                }

            } elseif ($inquiryPayment == '022') {
                // inquiry with
                $tCash = TcashTransactions::payment($request, $paymentId);
                if ($tCash->isSuccess) {
                    $tcashRes = $tCash->data['trx_id'].":".$tCash->data['bill_ref'].":".$tCash->data['notif'];
                } else {
                    return $response;
                }
            } elseif ($inquiryPayment == '023') {
                // inquiry reversal with
                $tCash = TcashTransactions::paymentReversal($request, $paymentId);
                if ($tCash->isSuccess) {
                    $tcashRes = $tCash->data['trx_id'].":".$tCash->data['bill_ref'].":".$tCash->data['notif'];
                } else {
                    return $response;
                }
            } else {
                $response = "01:Type Transaction Not Valid";
            }

        }

        Helper::LogPayment("End Callback Payment Notifications ".json_encode($tcashRes));

        $response = "00:".$tcashRes;
        return $response;
    }

    public function tcashSuccess(Request $request)
    {
        $refNum = $request->input('refNum', null);
        $trxId = $request->input('trxId', null);
        $src = $request->input('src', null);
        
        $param = [ 
            'status' => 'success',
            'payChannel' => 'TCASH',
        ];

        $dataReq = [
            'refNum' => $refNum,
            'trxId' => $trxId,
            'src' => $src,
        ];

        $clientTransactionDb = ClientTransaction::with('tCash')
                                                ->where('payment_id', $trxId)
                                                ->first();
        
        Helper::LogPayment("Inquiry $refNum $trxId",'tcash','inquiry');

        if ($clientTransactionDb->status == 'CREATED' && $clientTransactionDb->tCash->ref_number == $refNum) {
            
            Helper::LogPayment('Begin Inquiry to tcash','tcash','inquiry');

            if (isset($refNum)) {
                $encodedData = json_encode($dataReq);
                Helper::LogPayment("Update DB with result $encodedData",'tcash','inquiry');
            
                DB::beginTransaction();
                $tcashDb = TcashTransactions::where('ref_number', $refNum)->first();
                $tcashDb->last_payment_amount = $tcashDb->transaction_amount;
                $tcashDb->param_payment       = $encodedData;
                $tcashDb->payment_date        = date('Y-m-d H:i:s');
                $tcashDb->status              = "Success";
                $tcashDb->save();

                 // update transactionDB
                 $paymentAmount = $tcashDb->transaction_amount;
                 $clientTransactionDb = ClientTransaction::find($clientTransactionDb->id);
                 $clientTransactionDb->status = 'PAID';
                 $clientTransactionDb->total_payment = $paymentAmount;
                 $clientTransactionDb->last_payment = $paymentAmount;
                 $clientTransactionDb->save();
                 
                 $clientTransactionId = $clientTransactionDb->id;
                 Helper::LogPayment("Update Transaction $clientTransactionId to 'PAID",'tcash','inquiry');
 
                 // insert history
                 ClientTransactionHistory::insertHistory($clientTransactionId,'PAID','PAID',$paymentAmount);
                 Helper::LogPayment("Insert History Transaction 'PAID",'tcash','inquiry');
 
                 // update client transaction detail to paid
                 $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
                 if ($clientTransactionDetailDb){
                     $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
                     $clientTransactionDetailDb->paid_amount = $paymentAmount;
                     $clientTransactionDetailDb->status = 'PAID';
                     $clientTransactionDetailDb->save();
                 }
                DB::commit();

                // Send Client Callback to PopSend
                dispatch(new SendClientCallback($clientTransactionDb));
                return view('callback.landing', $param);

            } else {
                $errorMsg = $inquiry->errorMsg;
                Helper::LogPayment('Inquiry Failed. '.$errorMsg,'tcash','inquiry');
            }       
        }
        return view('callback.landing', $param);
    }

    public function tcashFailed(Request $request)
    {
        $trxId = $request->input('trxId', null);
        $src = $request->input('src', null);
        $param = [ 
            'status' => 'failed',
            'payChannel' => 'TCASH',
        ];

        $dataReq = [
            'trxId' => $trxId,
            'src' => $src,
        ];

        $clientTransactionDb = ClientTransaction::with('tCash')
                                                ->where('payment_id', $trxId)
                                                ->first();

        Helper::LogPayment("Failed Payment $trxId",'tcash','inquiry');

        $encodedData = json_encode($dataReq);
        Helper::LogPayment("Update DB with result $encodedData",'tcash','inquiry');

        if ((($clientTransactionDb->status != 'FAILED') ||  ($clientTransactionDb->status != 'PAID')) && ($clientTransactionDb->status == 'CREATED')) {
            
                 DB::beginTransaction();
                $tcashDb = TcashTransactions::where('client_transactions_id', $clientTransactionDb->id)->first();
                $tcashDb->last_payment_amount = $tcashDb->transaction_amount;
                $tcashDb->param_payment       = $encodedData;
                $tcashDb->payment_date        = date('Y-m-d H:i:s');
                $tcashDb->status              = "Failed";
                $tcashDb->save();
        
                // update transactionDB
                $paymentAmount = $tcashDb->transaction_amount;
                $clientTransactionDb = ClientTransaction::find($clientTransactionDb->id);
                $clientTransactionDb->status = 'FAILED';
                $clientTransactionDb->total_payment = $paymentAmount;
                $clientTransactionDb->last_payment = $paymentAmount;
                $clientTransactionDb->save();
                
                $clientTransactionId = $clientTransactionDb->id;
                Helper::LogPayment("Update Transaction $clientTransactionId to 'FAILED",'tcash','inquiry');
        
                // insert history
                ClientTransactionHistory::insertHistory($clientTransactionId,'FAILED','FAILED',$paymentAmount);
                Helper::LogPayment("Insert History Transaction 'FAILED",'tcash','inquiry');
        
                // update client transaction detail to paid
                $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
                if ($clientTransactionDetailDb){
                    $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
                    $clientTransactionDetailDb->paid_amount = $paymentAmount;
                    $clientTransactionDetailDb->status = 'FAILED';
                    $clientTransactionDetailDb->save();
                }

            DB::commit();

            // Send Client Callback to PopSend
            dispatch(new SendClientCallback($clientTransactionDb)); 
            return view('callback.landing', $param);              
        }

        return view('callback.landing', $param);              
    }

    public function midtransNotify(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = true;
        $req = $request->getContent();
        $midtransTrx = new MidtransTransactions();
        $updatePayment = $midtransTrx->notification($request);
        Helper::LogPayment("notification from midtrans $req ",'midtrans','notification');

        if ($updatePayment->isSuccess) {
            $response->isSuccess = true;
            return response()->json($response);
        } 
        return response()->json($response);
    }

    /*======================Private Function======================*/
    /**
     * private post molpay IPN
     * @param array $postData
     */
    private function molPayIPN($postData=[]){
        $postdata = implode("&",$postData);
        $url = "https://www.onlinepayment.com.my/MOLPay/API/chkstat/returnipn.php";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST , 1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS , $postData );
        curl_setopt($ch, CURLOPT_URL , $url );
        curl_setopt($ch, CURLOPT_HEADER , 1 );
        curl_setopt($ch, CURLINFO_HEADER_OUT , TRUE );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1 );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , FALSE );
        $result = curl_exec( $ch );
        curl_close( $ch );
        return;
    }
}
