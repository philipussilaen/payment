<?php

namespace App\Http\Controllers;

use App\Http\Models\apiV1\ClientTransaction;
use App\Http\Models\apiV1\DokuTransaction;
use Illuminate\Http\Request;

class WebViewController extends Controller
{
    public function dokuHosted(Request $request,$paymentHased=null){
        if (empty($paymentHased)) return "Empty Parameter";
        $encryptedPaymentId = base64_decode($paymentHased);
        $paymentId = decrypt($encryptedPaymentId);

        // get transaction DB
        $clientTransactionDB = ClientTransaction::where('payment_id',$paymentId)->first();
        if (!$clientTransactionDB){
            return "Payment Not Found";
        }

        // get doku transaction
        $dokuTransactionDb = DokuTransaction::where('client_transactions_id',$clientTransactionDB->id)->first();
        if (!$dokuTransactionDb){
            return "Doku Not Found";
        }
        // get form parameter
        $formParameter = $dokuTransactionDb->form_params;
        if (empty($formParameter)){
            return "Empty Parameter";
        }
        $formParameter = json_decode($formParameter);
        // get doku suite url
        $dokuUrl = env('DOKU_SUITE');
        // parse to view
        $data = [];
        $data['formParameter'] = $formParameter;
        $data['dokuUrl'] = $dokuUrl;

        return view('doku.doku-suite',$data);
    }
}
