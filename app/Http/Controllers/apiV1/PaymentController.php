<?php

namespace App\Http\Controllers\apiV1;

use App\Http\Helper\apiV1\ApiHelper;
use App\Http\Helper\Helper;
use App\Http\Models\apiV1\BNIPrefixToken;
use App\Http\Models\apiV1\BNIVirtualAccount;
use App\Http\Models\apiV1\BNIYap;
use App\Http\Models\apiV1\ClientTransaction;
use App\Http\Models\apiV1\ClientTransactionHistory;
use App\Http\Models\apiV1\CompanyAccessToken;
use App\Http\Models\apiV1\DokuTransaction;
use App\Http\Models\apiV1\EMoneyCard;
use App\Http\Models\apiV1\Location;
use App\Http\Models\apiV1\MolPayChannel;
use App\Http\Models\apiV1\MolPayTransaction;
use App\Http\Models\apiV1\OttopayTransactions;
use App\Http\Models\apiV1\TcashTransactions;
use App\Http\Models\apiV1\MidtransTransactions;
use App\Http\Models\apiV1\NotificationSMS;
use App\Http\Models\apiV1\PaymentChannel;
use App\Http\Models\apiV1\TokenChannelPrivilege;
use App\Http\Libraries\OttoPayAPI;
use App\Jobs\NotificationSendSMS;
use App\Jobs\SendClientCallback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Client;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class PaymentController extends Controller
{
    /**
     * Get Payment Method
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPaymentMethod(Request $request){
        $code = 400;
        $message = null;

        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            return ApiHelper::buildResponse($code, $message);
        }
        $token = $request->input('token');
        $session = $request->input('session_id');
        // check payment method available based on token provided
        $tokenChannelPrivilegeDb = CompanyAccessToken::getPaymentChannels($token,$input);

        $availableChannel = [];
        foreach ($tokenChannelPrivilegeDb as $item) {
            $tmp = new \stdClass();
            $tmp->type = $item->type;
            $tmp->name = $item->name;
            $tmp->code = $item->code;
            $tmp->text = $item->text;
            $tmp->description = $item->description;
            $tmp->long_description = $item->long_description;
            $tmp->image_url = null;

            $image = $item->image_url;
            if (!empty($image)) $tmp->image_url = url($image);

            $status = 'CLOSE';
            if ($item->status=='OPEN' && $item->vendor_status == 'OPEN' && $item->privileges_status == 'OPEN') $status = 'OPEN';
            $tmp->status = $status;
            $availableChannel[] = $tmp;
        }

        if (empty($availableChannel)){
            $message = 'Not Available';
            return ApiHelper::buildResponse($code,$message);
        }
        $code = 200;
        return ApiHelper::buildResponse($code,$message,$availableChannel);
    }

    /**
     * Create Payment Based On Method
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPayment(Request $request){
        $code = 400;
        $message = null;
       
        //required param list
        $required = ['method_code','transaction_id','customer_name','amount'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            return ApiHelper::buildResponse($code, $message);
        }

        $methodCode = $request->input('method_code');
        $token = $request->input('token');
        $sessionId = $request->input('session_id');

        /*if ($sessionId != session('session_id')){
            $message = 'Session Mismatch';
            return ApiHelper::buildResponse($code,$message);
        }*/

        // check if method exist
        $paymentChannelDb = PaymentChannel::where('code',$methodCode)->first();
        if (!$paymentChannelDb){
            $message = 'Invalid Method';
            return ApiHelper::buildResponse($code,$message);
        }
        // check if token authorized
        $tmpParam = [];
        $tmpParam['method_code'] = $methodCode;
        $companyAccessDb = CompanyAccessToken::getPaymentChannels($token,$tmpParam);
        $selectedMethod = $companyAccessDb->first();
        if (!$selectedMethod){
            $message = 'Unauthorized Method';
            return ApiHelper::buildResponse($code,$message);
        }
        // dd('a');
        $status = 'CLOSE';
        if ($selectedMethod->status=='OPEN' && $selectedMethod->vendor_status == 'OPEN' && $selectedMethod->privileges_status == 'OPEN') $status = 'OPEN';
        if ($status=='CLOSE'){
            $message = 'Payment Method Temporarily Closed';
            return ApiHelper::buildResponse($code,$message);
        }

        DB::beginTransaction();
        // create location
        Helper::LogPayment("Begin Create Location");
        $locationDb = Location::createLocation($request);
        if (!$locationDb->isSuccess){
            $message = $locationDb->errorMsg;
            Helper::LogPayment("End Create Location. Failed: ".$message);
            return ApiHelper::buildResponse($code,$message);
        }
        $locationId = $locationDb->locationId;
        Helper::LogPayment("Finish Create Location");
        DB::commit();

        DB::beginTransaction();
        Helper::LogPayment("Begin Create Transaction");
        // create Client Transaction
        $clientTransactionDb = ClientTransaction::createTransaction($request,$locationId);
        if (!$clientTransactionDb->isSuccess){
            $message = $clientTransactionDb->errorMsg;
            Helper::LogPayment("End Create Transaction. Failed: ".$message);
            return ApiHelper::buildResponse($code,$message);
        }
        $clientTransactionId = $clientTransactionDb->transactionId;
        Helper::LogPayment("End Create Transaction. Transaction Id : $clientTransactionId");

        $isSuccess = false;
        $response = null;
        Helper::LogPayment("Begin Create Payment Method : ".$methodCode);
        switch ($methodCode){
            case 'BNI-VA':
                $createBNIVa = $this->createBNIVa($request, $clientTransactionId);
                if (!$createBNIVa->isSuccess){
                    $message = $createBNIVa->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createBNIVa->data;
                }
                break;
            case 'MP-FPX' :
                $molPayChannelCode = 'fpx';
                $createMolPayURL = $this->createMolPay($request, $clientTransactionId,$molPayChannelCode);
                if (!$createMolPayURL->isSuccess){
                    $message = $createMolPayURL->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createMolPayURL->data;
                }
                break;
            case 'MP-MB2U' :
                $molPayChannelCode = 'maybank2u';
                $createMolPayURL = $this->createMolPay($request, $clientTransactionId,$molPayChannelCode);
                if (!$createMolPayURL->isSuccess){
                    $message = $createMolPayURL->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createMolPayURL->data;
                }
                break;
            case 'MP-CIMB' :
                $molPayChannelCode = 'cimb';
                $createMolPayURL = $this->createMolPay($request, $clientTransactionId,$molPayChannelCode);
                if (!$createMolPayURL->isSuccess){
                    $message = $createMolPayURL->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createMolPayURL->data;
                }
                break;
            case 'MP-RHB' :
                $molPayChannelCode = 'rhb';
                $createMolPayURL = $this->createMolPay($request, $clientTransactionId,$molPayChannelCode);
                if (!$createMolPayURL->isSuccess){
                    $message = $createMolPayURL->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createMolPayURL->data;
                }
                break;
            case 'MP-AMB' :
                $molPayChannelCode = 'amb';
                $createMolPayURL = $this->createMolPay($request, $clientTransactionId,$molPayChannelCode);
                if (!$createMolPayURL->isSuccess){
                    $message = $createMolPayURL->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createMolPayURL->data;
                }
                break;
            case 'MP-AFFIN' :
                $molPayChannelCode = 'affin-epg';
                $createMolPayURL = $this->createMolPay($request, $clientTransactionId,$molPayChannelCode);
                if (!$createMolPayURL->isSuccess){
                    $message = $createMolPayURL->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createMolPayURL->data;
                }
                break;
            case 'MP-HLB' :
                $molPayChannelCode = 'hlb';
                $createMolPayURL = $this->createMolPay($request, $clientTransactionId,$molPayChannelCode);
                if (!$createMolPayURL->isSuccess){
                    $message = $createMolPayURL->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createMolPayURL->data;
                }
                break;
            case 'MP-CC' :
                $molPayChannelCode = 'credit';
                $createMolPayURL = $this->createMolPay($request, $clientTransactionId,$molPayChannelCode);
                if (!$createMolPayURL->isSuccess){
                    $message = $createMolPayURL->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createMolPayURL->data;
                }
                break;
            case 'MP-7CASH' :
                $molPayChannelCode = 'cash';
                $createMolPayURL = $this->createMolPay($request, $clientTransactionId,$molPayChannelCode);
                if (!$createMolPayURL->isSuccess){
                    $message = $createMolPayURL->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createMolPayURL->data;
                }
                break;
            case 'MP-VACIMB' :
                $molPayChannelCode = 'CIMB-VA';
                $createMolPayURL = $this->createMolPay($request, $clientTransactionId,$molPayChannelCode);
                if (!$createMolPayURL->isSuccess){
                    $message = $createMolPayURL->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createMolPayURL->data;
                }
                break;
            case 'DOKU-MANDIRI':
                $createDokuVA = $this->createDokuVA($request, $clientTransactionId,$methodCode);
                if (!$createDokuVA->isSuccess){
                    $message = $createDokuVA->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createDokuVA->data;
                }
                break;
            case 'DOKU-PERMATA':
                $createDokuVA = $this->createDokuVA($request, $clientTransactionId,$methodCode);
                if (!$createDokuVA->isSuccess){
                    $message = $createDokuVA->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createDokuVA->data;
                }
                break;
            case 'DOKU-BCA':
                $createDokuVA = $this->createDokuVA($request, $clientTransactionId,$methodCode);
                if (!$createDokuVA->isSuccess){
                    $message = $createDokuVA->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createDokuVA->data;
                }
                break;
            case 'DOKU-ALFAMART':
                $createDokuVA = $this->createDokuVA($request, $clientTransactionId,$methodCode);
                if (!$createDokuVA->isSuccess){
                    $message = $createDokuVA->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createDokuVA->data;
                }
                break;
            case 'DOKU-INDOMART':
                $createDokuVA = $this->createDokuVA($request, $clientTransactionId,$methodCode);
                if (!$createDokuVA->isSuccess){
                    $message = $createDokuVA->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createDokuVA->data;
                }
                break;
            case 'DOKU-CREDIT':
                $createDokuVA = $this->createDokuDirect($request, $clientTransactionId,$methodCode);
                if (!$createDokuVA->isSuccess){
                    $message = $createDokuVA->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createDokuVA->data;
                }
                break;
            case 'DOKU-WALLET':
                $createDokuVA = $this->createDokuDirect($request, $clientTransactionId,$methodCode);
                if (!$createDokuVA->isSuccess){
                    $message = $createDokuVA->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createDokuVA->data;
                }
                break;
            case 'BNI-YAP':
                $createBNIYap = $this->createBNIYap($request, $clientTransactionId,$locationId);
                if (!$createBNIYap->isSuccess){
                    $message = $createBNIYap->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createBNIYap->data;
                }
                break;
            case 'MANDIRI-EMONEY':
                $createEmoney = $this->createEmoneyCard($request, $clientTransactionId);
                if (!$createEmoney->isSuccess){
                    $message = $createEmoney->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createEmoney->data;
                }
                break;

            case 'OTTO-QR':
                $createBillOttoPay = $this->createBillOttopay($request, $clientTransactionId);
                if (!$createBillOttoPay->isSuccess){
                    $message = $createBillOttoPay->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createBillOttoPay->data;
                }
                break;

            case 'TCASH':
                $billPaymentTcash = $this->tcashBillPayment($request, $clientTransactionId);
                if (!$billPaymentTcash->isSuccess){
                    $message = $billPaymentTcash->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $billPaymentTcash->data;
                }
                break;
            
            case 'TCASH-WEB':
                $tcashCheckout = $this->tcashBillPayment($request, $clientTransactionId);
                if (!$tcashCheckout->isSuccess){
                    $message = $tcashCheckout->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $tcashCheckout->data;
                }
                break;

            case 'MID-GOPAY':
                $gopay = $this->midtransTrx($request, $clientTransactionId, 'gopay');
                if (!$gopay->isSuccess){
                    $message = $gopay->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $gopay->data;
                }
                break;

            case 'MID-SNAP':
                $gopay = $this->midtransTrx($request, $clientTransactionId, 'snap');
                if (!$gopay->isSuccess){
                    $message = $gopay->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $gopay->data;
                }
                break;
            
            default:
                $message = 'Undefined Method Function';
        }

        if (!$isSuccess){
            Helper::LogPayment("End Create Payment Method. Failed : ".$message);
            DB::rollback();
            return ApiHelper::buildResponse($code,$message);
        }

        DB::commit();
        Helper::LogPayment("End Create Payment. Response : ".json_encode($response));
        $code = 200;
        return ApiHelper::buildResponse($code,$message,$response);
    }

    /**
     * Inquiry Payment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function inquiryPayment(Request $request){
        $code = 400;
        $message = null;

        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            return ApiHelper::buildResponse($code, $message);
        }

        $clientTransactionId = $request->input('transaction_id',null);
        $paymentId = $request->input('payment_id',null);

        if (empty($clientTransactionId) && empty($paymentId)){
            $message = "Client Transaction ID or Payment ID is Required";
            return ApiHelper::buildResponse($code,$message);
        }

        $paymentMethodCode = null;
        // get client transaction DB
        if (!empty($clientTransactionId)){
            $clientTransactionDb = ClientTransaction::where('transaction_id',$clientTransactionId)->first();
            if (!$clientTransactionDb){
                $message = 'Invalid Transaction Id';
                return ApiHelper::buildResponse($code,$message);
            }
            $paymentId = $clientTransactionDb->payment_id;
            $paymentMethodCode = $clientTransactionDb->payment_channel_code;
        } elseif (!empty($paymentId)){
            $clientTransactionDb = ClientTransaction::where('payment_id',$paymentId)->first();
            if (!$clientTransactionDb){
                $message = 'Invalid Transaction Id';
                return ApiHelper::buildResponse($code,$message);
            }
            $paymentId = $clientTransactionDb->payment_id;
            $paymentMethodCode = $clientTransactionDb->payment_channel_code;
        }

        $isSuccess = false;
        $response = null;
        
        DB::beginTransaction();
        switch ($paymentMethodCode){
            case 'BNI-VA' :
                $createBNIVa = $this->inquiryBNIVa($request, $paymentId);
                if (!$createBNIVa->isSuccess){
                    $message = $createBNIVa->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createBNIVa->data;
                }
                break;
            case 'BNI-YAP':
                $inquiryBNIYap = $this->inquiryBNIYap($request,$paymentId);
                if (!$inquiryBNIYap->isSuccess){
                    $message = $inquiryBNIYap->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $inquiryBNIYap->data;
                }
                break;
            case 'OTTO-QR':
                $inquiryOttoPay = $this->inquiryOttoPay($request,$paymentId);
                if (!$inquiryOttoPay->isSuccess){
                    $message = $inquiryOttoPay->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $inquiryOttoPay->data;
                }
                break;

            case 'TCASH': 
                $inquiryTcash = $this->tcashInquiry($request,$paymentId);
                if (!$inquiryTcash->isSuccess) {
                    $message = $inquiryTcash->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiryTcash->data;
                }
                break;

            case 'TCASH-WEB': 
                $inquiryTcash = $this->tcashInquiry($request,$paymentId);
                if (!$inquiryTcash->isSuccess) {
                    $message = $inquiryTcash->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiryTcash->data;
                }
                break;

            case 'DOKU-MANDIRI': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'doku');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'DOKU-PERMATA': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'doku');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'DOKU-BCA': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'doku');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'DOKU-ALFAMART': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'doku');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'DOKU-INDOMART': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'doku');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'DOKU-CREDIT': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'doku');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'DOKU-WALLET': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'doku');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'MP-FPX': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'molpay');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;
            
            case 'MP-MB2U': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'molpay');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'MP-CIMB': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'molpay');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'MP-RHB': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'molpay');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;
            
            case 'MP-AMB': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'molpay');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'MP-AFFIN': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'molpay');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'MP-HLB': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'molpay');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;
            
            case 'MP-CC': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'molpay');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'MP-7CASH': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'molpay');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'MP-VACIMB': 
                $inquiry = $this->globalInquiry($request,$paymentId, $paymentMethodCode, 'molpay');
                if (!$inquiry->isSuccess) {
                    $message = $inquiry->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiry->data;
                }
                break;

            case 'MID-SNAP': 
                $inquiryMidsnap = $this->midtransInquiry($request,$paymentId);
                if (!$inquiryMidsnap->isSuccess) {
                    $message = $inquiryMidsnap->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiryMidsnap->data;
                }
                break;

            case 'MID-GOPAY': 
                $inquiryMidsnap = $this->midtransInquiry($request,$paymentId);
                if (!$inquiryMidsnap->isSuccess) {
                    $message = $inquiryMidsnap->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiryMidsnap->data;
                }
                break;

            case 'MANDIRI-EMONEY': 
                $inquiryMandiri = $this->mandiriEmoney($request,$paymentId);
                if (!$inquiryMandiri->isSuccess) {
                    $message = $inquiryMandiri->errorMsg;                    
                } else {
                    $isSuccess = true;
                    $response = $inquiryMandiri->data;
                }
                break;

            default:
                $message = 'Undefined Method Function';
        }

        if (!$isSuccess){
            DB::rollback();
            return ApiHelper::buildResponse($code,$message);
        }

        DB::commit();
        $code = 200;
        return ApiHelper::buildResponse($code,$message,$response);
    }

    /**
     * Paid Payment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paidPayment(Request $request){
        $code = 400;
        $message = null;

        //required param list
        $required = ['payment_id','method_code'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            return ApiHelper::buildResponse($code, $message);
        }

        $methodCode = $request->input('method_code');
        $token = $request->input('token');
        $paymentId = $request->input('payment_id');

        Helper::LogPayment("Begin Paid Payment $methodCode, $paymentId");

        // get client transaction
        $clientTransactionDb = ClientTransaction::where('payment_id',$paymentId)->first();
        if (!$clientTransactionDb){
            $message = 'Invalid Payment ID';
            Helper::LogPayment("Failed $message");
            return ApiHelper::buildResponse(400,$message);
        }

        if ($clientTransactionDb->payment_channel_code != $methodCode){
            $message = 'Invalid Method';
            Helper::LogPayment("Failed $message");

            return ApiHelper::buildResponse(400,$message);
        }
        $clientTransactionId = $clientTransactionDb->id;
        DB::beginTransaction();
        $isSuccess = false;
        switch ($methodCode){
            case 'MANDIRI-EMONEY' :
                $createEmoney = $this->paidEmoneyCard($request, $clientTransactionId);
                if (!$createEmoney->isSuccess){
                    $message = $createEmoney->errorMsg;
                } else {
                    $isSuccess = true;
                    $response = $createEmoney->data;
                }
                break;
            default :
                $message = 'Invalid Code';
                break;
        }

        if (!$isSuccess){
            Helper::LogPayment("Failed $message");
            DB::rollback();
            return ApiHelper::buildResponse(400,$message);
        }

        DB::commit();
        Helper::LogPayment("Dispatch");
        dispatch(new SendClientCallback($clientTransactionDb));

        Helper::LogPayment("End Paid Payment. Response : ".json_encode($response));
        $code = 200;
        return ApiHelper::buildResponse($code,$message,$response);
    }

    /*============= Private Function =============*/

    /**
     * Create BNI Virtual Account
     * @param Request $request
     * @param $clientTransactionId
     * @return \stdClass
     */
    private function createBNIVa(Request $request, $clientTransactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        Helper::LogPayment("Begin Create BNI Virtual Account");

        //required param list
        $required = ['billing_type'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errorMsg = $message;
            return $response;
        }

        $billingType = $request->input('billing_type');
        $virtualAccount = $request->input('virtual_account',null);
        $dateTimeExpired = $request->input('datetime_expired',null);
        $token = $request->input('token');
        // available billing type
        $availableBillingType = [
            'open' => 'o',
            'fixed' => 'c'
        ];

        $billingTypePrefix = [
            'open' => '0',
            'fixed' => '1'
        ];

        if (!array_key_exists($billingType,$availableBillingType)){
            $message = 'Invalid Billing Type';
            $response->errorMsg = $message;
            return $response;
        }
        $billingCode = $availableBillingType[$billingType];

        // check VA
        // if exist
        if (!empty($virtualAccount)){
            if (strlen($virtualAccount) > 10){
                $response->errorMsg = 'Virtual Account Number must less than 10 character';
                return $response;
            }
            if (!is_numeric($virtualAccount)){
                $response->errorMsg = "Virtual Account Number must be numeric";
                return $response;
            }
        }
        // create VA if not exist
        else {
            // generate
            $virtualAccount = Helper::generateRandomString(10,'Numeric');
        }

        // get client id
        $clientId = env('BNI_CLIENT_ID','261');
        $firstDigit = "8$clientId";
        // get company access prefix
        $BNIPrefixTokenDb =BNIPrefixToken::join('company_access_tokens','company_access_tokens.id','=','bni_virtual_prefix_token.company_access_tokens_id')
            ->where('token',$token)
            ->first();
        if (!$BNIPrefixTokenDb){
            $response->errorMsg = 'Company Token Prefix is not available';
            return $response;
        }
        $companyPrefix = $BNIPrefixTokenDb->prefix;
        $billingTypeNumber = $billingTypePrefix[$billingType];
        $virtualAccount = $firstDigit.$companyPrefix.$billingTypeNumber.$virtualAccount;

        Helper::LogPayment("Virtual Number ".$virtualAccount);

        // create datetime expired
        if (empty($dateTimeExpired)){
            $dateTimeExpired = date('c',strtotime('+ 6hours'));
        } else {
            // check if date time expired less than now time
            $nowTime = time();
            $expiredTime = strtotime($dateTimeExpired);
            if ($expiredTime < $nowTime){
                $response->errorMsg = 'Expired Datetime less than now';
                return $response;
            }
        }

        Helper::LogPayment("Expire Date $dateTimeExpired");

        // create VA BNI and DB
        $BNIVirtualDb = BNIVirtualAccount::createVA($clientTransactionId,$billingCode,$virtualAccount,$dateTimeExpired);
        if (!$BNIVirtualDb->isSuccess){
            Helper::LogPayment("End Create BNI Virtual Account. Failed ".$BNIVirtualDb->errorMsg);
            $response->errorMsg = $BNIVirtualDb->errorMsg;
            return $response;
        }
        $BNIVirtualId = $BNIVirtualDb->BNIVaId;
        $BNIVirtualDb = BNIVirtualAccount::find($BNIVirtualId);

        // update transaction
        $clientTransactionDB = ClientTransaction::find($clientTransactionId);
        $clientTransactionDB->payment_type = $billingType;
        $clientTransactionDB->total_amount = $BNIVirtualDb->transaction_amount;
        $clientTransactionDB->payment_channel_code = 'BNI-VA';
        $clientTransactionDB->save();

        $data = new \stdClass();
        $data->virtual_account_number = $BNIVirtualDb->virtual_account_number;
        $data->transaction_id = $BNIVirtualDb->clientTransaction->transaction_id;
        $data->payment_id = $BNIVirtualDb->clientTransaction->payment_id;
        $data->datetime_expired = $BNIVirtualDb->datetime_expired;

        Helper::LogPayment("End Create BNI Virtual Account ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Inquiry BNI Virtual Account
     * @param Request $request
     * @param $paymentId
     * @return \stdClass
     */
    private function inquiryBNIVa(Request $request,$paymentId){
        Helper::LogPayment("Begin Inquiry BNI Virtual Account");
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $BNIVirtualDb = BNIVirtualAccount::inquiryVA($paymentId);
        if (!$BNIVirtualDb->isSuccess){
            Helper::LogPayment("End Create BNI Virtual Account. Failed ".$BNIVirtualDb->errorMsg);
            $response->errorMsg = $BNIVirtualDb->errorMsg;
            return $response;
        }

        $BNIVirtualId = $BNIVirtualDb->BNIVaId;
        $BNIVirtualDb = BNIVirtualAccount::find($BNIVirtualId);

        $availableBillingType = [
            'o' => 'open',
            'c' => 'fixed'
        ];

        $billingType = $availableBillingType[$BNIVirtualDb->billing_type];

        // update transaction
        $clientTransactionDB = ClientTransaction::find($BNIVirtualDb->client_transactions_id);
        $clientTransactionDB->payment_type = $billingType;
        $clientTransactionDB->total_amount = $BNIVirtualDb->transaction_amount;
        $clientTransactionDB->total_payment = $BNIVirtualDb->payment_amount;
        $clientTransactionDB->last_payment = $BNIVirtualDb->last_payment_amount;
        $clientTransactionDB->save();

        $data = new \stdClass();
        $data->virtual_account_number = $BNIVirtualDb->virtual_account_number;
        $data->transaction_id = $clientTransactionDB->id;
        $data->payment_id = $clientTransactionDB->payment_id;
        $data->datetime_expired = $BNIVirtualDb->datetime_expired;

        Helper::LogPayment("End Inquiry BNI Virtual Account ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create MolPay
     * @param Request $request
     * @param $clienTransactionId
     * @param string $molPayChannelCode
     * @return \stdClass
     */
    private function createMolPay(Request $request,$clienTransactionId,$molPayChannelCode='credit'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        Helper::LogPayment("Begin Create MolPay");

        //required param list
        $required = ['customer_email','customer_phone'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errorMsg = $message;
            return $response;
        }

        $amount = $request->input('amount');

        // check channel code
        $molpayChannelDb = MolPayChannel::where('code',$molPayChannelCode)->first();
        if (!$molpayChannelDb){
            $response->errorMsg = 'Invalid MolPay Channel Code';
            return $response;
        }
        if ($molpayChannelDb->status!='OPEN'){
            $response->errorMsg = 'MolPay Channel Closed';
            return $response;
        }
        $minimumAmount = $molpayChannelDb->range_amount;
        $downtime = $molpayChannelDb->downtime;
        if (!empty($minimumAmount)){
            // check minimum amount
            if ($amount < $minimumAmount){
                $response->errorMsg = 'Minimum Amount must be '.$minimumAmount;
                return $response;
            }
        }
        if (!empty($downtime)){
            $tmTime = explode('-',$downtime);
            $startTime = date('H:i',strtotime($tmTime[0]));
            $endTime = date('H:i',strtotime($tmTime[1]));
            $nowTime = date('H:i');
            if ($nowTime > $startTime && $nowTime < $endTime){
                $response->errorMsg = 'Downtime Payment';
                return $response;
            }
        }

        Helper::LogPayment("Create URL for MolPay");
        $molPay = MolPayTransaction::createMolPayTransaction($request,$clienTransactionId,$molPayChannelCode);
        Helper::LogPayment("Response ".json_encode($molPay));

        $data = new \stdClass();
        $data->url =  $molPay->molpayUrl;

        // update transaction
        $clientTransactionDB = ClientTransaction::find($clienTransactionId);
        $clientTransactionDB->payment_channel_code = $request->input('method_code');
        $clientTransactionDB->save();

        Helper::LogPayment("End Create URL for MolPay ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create Doku VA
     * @param Request $request
     * @param $clientTransactionId
     * @param string $dokuChannelCode
     * @return \stdClass
     */
    private function createDokuVA(Request $request, $clientTransactionId, $dokuChannelCode='doku-permata'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        Helper::LogPayment("Begin Create Doku VA");

        //required param list
        $required = ['billing_type'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errorMsg = $message;
            return $response;
        }
        $dateTimeExpired = $request->input('datetime_expired',null);
        $virtualAccount = $request->input('virtual_account',null);
        $billingType = $request->input('billing_type','fixed');
        $token = $request->input('token');

        if (!in_array($billingType,['fixed','open'])){
            $response->errorMsg = 'Invalid Billing Type';
            return $response;
        }

        $availableVA = DokuTransaction::$availableVA;
        if (!in_array($dokuChannelCode,$availableVA)){
            $response->errorMsg = 'Invalid Doku VA Channel Code';
            return $response;
        }

        // create expired minute
        $expiredMinute = 360;
        if (!empty($dateTimeExpired)){
            $nowTime = time();
            $expiredTime = strtotime($dateTimeExpired);
            if ($expiredTime < $nowTime){
                $response->errorMsg = 'Expired Datetime less than now';
                return $response;
            }
            // count different
            $expiredMinute = round(abs($expiredTime-$nowTime)/60);
            $expiredMinute = (int)$expiredMinute;
        } else {
            $expiredMinute = 360;
        }

        if (!empty($virtualAccount)){
            if (strlen($virtualAccount) > 10){
                $response->errorMsg = 'Virtual Account Number must less than 10 character';
                return $response;
            }
            if (!is_numeric($virtualAccount)){
                $response->errorMsg = "Virtual Account Number must be numeric";
                return $response;
            }
            $BNIPrefixTokenDb =BNIPrefixToken::join('company_access_tokens','company_access_tokens.id','=','bni_virtual_prefix_token.company_access_tokens_id')
                ->where('token',$token)
                ->first();
            if (!$BNIPrefixTokenDb){
                $response->errorMsg = 'Company Token Prefix is not available';
                return $response;
            }
            $companyPrefix = $BNIPrefixTokenDb->prefix;
            $virtualAccount = $companyPrefix.$virtualAccount;
        }

        Helper::LogPayment("Create Doku Transaction VA");
        $dokuTransaction = DokuTransaction::createDokuVATransaction($clientTransactionId,$dokuChannelCode,$expiredMinute,$billingType,$virtualAccount);
        Helper::LogPayment("Response ".json_encode($dokuTransaction));

        if (!$dokuTransaction->isSuccess){
            Helper::LogPayment("Failed Doku Transaction VA ".$dokuTransaction->errorMsg);
            $response->errorMsg = $dokuTransaction->errorMsg;
            return $response;
        }

        $dokuTransactionId = $dokuTransaction->dokuTransactionId;
        $dokuTransactionDb = DokuTransaction::find($dokuTransactionId);

        $paymentCode = $dokuTransactionDb->virtual_number;

        // update transaction
        $clientTransactionDB = ClientTransaction::find($clientTransactionId);
        $clientTransactionDB->payment_channel_code = $request->input('method_code');
        $clientTransactionDB->payment_type = $billingType;
        $clientTransactionDB->total_amount = $dokuTransactionDb->transaction_amount;
        $clientTransactionDB->save();

        $data = new \stdClass();
        $data->virtual_account_number = $paymentCode;
        $data->transaction_id = $clientTransactionDB->id;
        $data->payment_id = $clientTransactionDB->payment_id;
        $data->datetime_expired = $dateTimeExpired;

        Helper::LogPayment("End Create Doku VA ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create Doku Hosted / Direct
     * @param Request $request
     * @param $clientTransactionId
     * @param string $dokuChannelCode
     * @return \stdClass
     */
    private function createDokuDirect(Request $request,$clientTransactionId,$dokuChannelCode='DOKU-WALLET'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        Helper::LogPayment("Begin Create Doku Direct");

        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errorMsg = $message;
            return $response;
        }
        $dateTimeExpired = $request->input('datetime_expired',null);


        $availableDirect = DokuTransaction::$availableDirect;
        if (!array_key_exists($dokuChannelCode,$availableDirect)){
            $response->errorMsg = 'Invalid Doku VA Channel Code';
            return $response;
        }

        // create expired minute
        $expiredMinute = 360;
        if (!empty($dateTimeExpired)){
            $nowTime = time();
            $expiredTime = strtotime($dateTimeExpired);
            if ($expiredTime < $nowTime){
                $response->errorMsg = 'Expired Datetime less than now';
                return $response;
            }
            // count different
            $expiredMinute = round(abs($expiredTime-$nowTime)/60);
            $expiredMinute = (int)$expiredMinute;
        } else {
            $expiredMinute = 360;
        }

        Helper::LogPayment("Create Doku Transaction Direct");
        $dokuTransaction = DokuTransaction::createDokuHostedTransaction($clientTransactionId,$dokuChannelCode,$expiredMinute);
        Helper::LogPayment("Response ".json_encode($dokuTransaction));

        if (!$dokuTransaction->isSuccess){
            Helper::LogPayment("Failed Doku Transaction Direct ".$dokuTransaction->errorMsg);
            $response->errorMsg = $dokuTransaction->errorMsg;
            return $response;
        }

        $dokuTransactionId = $dokuTransaction->dokuTransactionId;
        $dokuTransactionDb = DokuTransaction::find($dokuTransactionId);

        // update transaction
        $clientTransactionDB = ClientTransaction::find($clientTransactionId);
        $clientTransactionDB->payment_channel_code = $request->input('method_code');
        $clientTransactionDB->save();

        $paymentId = $clientTransactionDB->payment_id;

        $encryptedPaymentId = base64_encode(encrypt($paymentId));
        $url = url('webview/doku')."/$encryptedPaymentId";

        $data = new \stdClass();
        $data->url = $url;

        Helper::LogPayment("End Create Doku Direct ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create BNI Yap
     * @param Request $request
     * @param $clientTransactionId
     * @param null $locationId
     * @return \stdClass
     */
    private function createBNIYap(Request $request,$clientTransactionId,$locationId=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        Helper::LogPayment("Begin Create BNI YAP");

        //required param list
        $required = ['location_type','location_name'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errorMsg = $message;
            return $response;
        }

        $amount = $request->input('amount');

        Helper::LogPayment("Create QR for BNI Yap");
        $bniYap = BNIYap::createYapTransaction($request,$clientTransactionId);
        Helper::LogPayment("Response ".json_encode($bniYap));

        if (!$bniYap->isSuccess){
            Helper::LogPayment("Failed Create QR BNI $bniYap->errorMsg");
            $response->errorMsg = $bniYap->errorMsg;
            return $response;
        }
        $bniYapId = $bniYap->bniYapId;

        // get BNI Yap from ID
        $bniYapDb = BNIYap::find($bniYapId);

        // create QR Image
        $data = new \stdClass();
        $data->payment_id = $bniYapDb->clientTransaction->payment_id;
        $data->transaction_id = $bniYapDb->clientTransaction->transaction_id;
        $data->status = $bniYapDb->clientTransaction->status;
        $data->url = $bniYap->url;
        $data->expired_datetime = date('Y-m-d H:i:s',strtotime($bniYapDb->expired_datetime));

        // update transaction
        $clientTransactionDB = ClientTransaction::find($clientTransactionId);
        $clientTransactionDB->payment_channel_code = $request->input('method_code');
        $clientTransactionDB->save();

        Helper::LogPayment("End Create QR for BNI YAP ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * @param Request $request
     * @param $paymentId
     * @return \stdClass
     */
    private function inquiryBNIYap(Request $request, $paymentId){
        Helper::LogPayment("Begin Inquiry BNI Yap!");
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $clientTransactionDb = ClientTransaction::with('BNIYap')
            ->where('payment_id',$paymentId)
            ->first();
        if (!$clientTransactionDb){
            $response->errorMsg = 'Client Transaction Not Found';
            return $response;
        }

        $bniYapDb = $clientTransactionDb->BNIYap;
        if (!$bniYapDb){
            $response->errorMsg = 'Invalid Payment ID and Method';
            return $response;
        }

        // inquiry with
        $bniYap = BNIYap::inquiryYap($paymentId);
        $clientTransactionDb = ClientTransaction::with('BNIYap')
            ->where('payment_id',$paymentId)
            ->first();

        // generate URL
        $fileName = "$paymentId.png";
        // set url
        $url = url("img/payment/yap/$fileName");

        // create QR Image
        $data = new \stdClass();
        $data->payment_id = $clientTransactionDb->payment_id;
        $data->transaction_id = $clientTransactionDb->transaction_id;
        $data->status = $clientTransactionDb->status;
        $data->url = $url;
        $data->expired_datetime = date('Y-m-d H:i:s',strtotime($bniYapDb->expired_datetime));

        if ($clientTransactionDb->status == 'PAID'){
            $transactionId = $clientTransactionDb->transaction_id;
            $amount = $clientTransactionDb->total_payment;
            $phone = $clientTransactionDb->customer_phone;
            $locationName = "PopBox";
            $date = date('Y-m-d H:i:s',strtotime($clientTransactionDb->updated_at));
            if (!empty($clientTransactionDb->location)){
                $locationName .="@ ".$clientTransactionDb->location->location_name;
            }
            $smsMessage = "Pembayaran $transactionId sukses Rp".number_format($amount)." pada $date, di $locationName";

            $notifDb = new NotificationSMS();
            $notifDb->module = 'YAP-Inquiry';
            $notifDb->client_transaction_id = $clientTransactionDb->id;
            $notifDb->vendor = 'global';
            $notifDb->to =$phone;
            $notifDb->sms = $smsMessage;
            $notifDb->status = 0;
            $notifDb->save();

            dispatch(new NotificationSendSMS($notifDb));
        }

        Helper::LogPayment("End Create QR for BNI YAP ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;
    }

     /**
     * @param Request $request
     * @param $paymentId
     * @return \stdClass
     */
    private function inquiryOttoPay(Request $request, $paymentId)
    {
        Helper::LogPayment("Begin Inquiry OttoPay!");
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $client = self::getClient($request->input('token'));
        
        $clientTransactionDb = ClientTransaction::with('OttoPay')
            ->where('payment_id',$paymentId)
            ->first();
        
        if (!$clientTransactionDb){
            $response->errorMsg = 'Client Transaction Not Found';
            return $response;
        }

        $ottoPayDb = $clientTransactionDb->OttoPay;
        if (!$ottoPayDb){
            $response->errorMsg = 'Invalid Payment ID and Method';
            return $response;
        }

        // CHECK PAYMENT STATUS
        $clientTransactionDb = ClientTransaction::with('OttoPay')
            ->where('payment_id',$paymentId)
            ->first();

            if (isset($clientTransactionDb->OttoPay) && $clientTransactionDb->OttoPay->status_payment != 'Success') {
            $ottoPayApi = new OttoPayAPI();
            $getStatusPayment = $ottoPayApi->inquiryOttoPay($paymentId, $client);

            if (isset($getStatusPayment->data->transactionStatus) && $getStatusPayment->data->transactionStatus == 'SUCCESS') {
                // inquiry with
                $ottoPay = OttopayTransactions::inquiryOttoPayment($request, $paymentId, $client);
                $clientTransactionDb = ClientTransaction::with('OttoPay')
                    ->where('payment_id',$paymentId)
                    ->first();
            }
        }

        $billingId = $clientTransactionDb->OttoPay->billing_id;
      
        // generate URL
        $fileName = "$billingId.png";
        // set url
        $url = url("img/payment/ottopay/$fileName");

        // create QR Image
        $data = new \stdClass();
        $data->payment_id = $clientTransactionDb->payment_id;
        $data->transaction_id = $clientTransactionDb->transaction_id;
        $data->status = $clientTransactionDb->status;
        $data->url = $url;
        $data->expired_datetime = null;

        Helper::LogPayment("End Create QR for OttoPay ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;

    }

    /*==========================================
    /* TCASH BILL PAYMENT
    /*=========================================*/

    public function tcashInquiry(Request $request, $paymentId)
    {
        Helper::LogPayment("Begin Inquiry tcash!");
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // CHECK PAYMENT AVAILABLE
        $clientTransactionDb = ClientTransaction::with('tCash')
            ->where('payment_id',$paymentId)
            ->first();

        if (!$clientTransactionDb){
            $response->errorMsg = 'Client Transaction Not Found';
            return $response;
        }

        $tcashDb = $clientTransactionDb->tCash;
        if (!$tcashDb){
            $response->errorMsg = 'Invalid Payment ID and Method';
            return $response;
        }

        // generate URL
        $fileName = "$paymentId.png";
        
        // set url
        $url = url("img/payment/tcash/$fileName");

        // create QR Image
        $data = new \stdClass();
        $data->payment_id = $clientTransactionDb->payment_id;
        $data->transaction_id = $clientTransactionDb->transaction_id;
        $data->status = $clientTransactionDb->status;
        $data->url = $url;
        $data->expired_datetime = null;

        Helper::LogPayment("End Create QR for OttoPay ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;

    }

    public function globalInquiry(Request $request, $paymentId, $paymentMethod, $paymentRelations)
    {
        Helper::LogPayment("Begin Inquiry $paymentMethod!");
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // CHECK PAYMENT AVAILABLE
        $clientTransactionDb = ClientTransaction::with($paymentRelations)
            ->where('payment_id',$paymentId)
            ->first();
        
        if (!$clientTransactionDb){
            $response->errorMsg = 'Client Transaction Not Found';
            return $response;
        }

        $doku = $clientTransactionDb->$paymentRelations;
        if (!$doku){
            $response->errorMsg = 'Invalid Payment ID and Method';
            return $response;
        }

        // create QR Image
        $data = new \stdClass();
        $data->payment_id = $clientTransactionDb->payment_id;
        $data->transaction_id = $clientTransactionDb->transaction_id;
        $data->status = $clientTransactionDb->status;
        $data->url = null;
        $data->expired_datetime = null;

        Helper::LogPayment("End Create QR for $paymentMethod ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;

    }    

    public function mandiriEmoney(Request $request, $paymentId)
    {
        Helper::LogPayment("Begin Inquiry MANDIRI-EMONEY!". json_encode($request->getContent()));
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // CHECK PAYMENT AVAILABLE
        $clientTransactionDb = ClientTransaction::with('mandiriEmoney')
            ->where('payment_id',$paymentId)
            ->first();

            if (!$clientTransactionDb){
            $response->errorMsg = 'Client Transaction Not Found';
            return $response;
        }

        $midSnap = $clientTransactionDb->mandiriEmoney;
        if (!$midSnap){
            $response->errorMsg = 'Invalid Payment ID and Method';
            return $response;
        }

        // create QR Image
        $data = new \stdClass();
        $data->payment_id = $clientTransactionDb->payment_id;
        $data->transaction_id = $clientTransactionDb->transaction_id;
        $data->status = $clientTransactionDb->status;
        $data->url = null;
        $data->expired_datetime = null;

        Helper::LogPayment("End Inquiry MANDIRI-EMONEY ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;
    }


    // MIDTRANS SNAP PAYMENT INQUIRY
    public function midtransInquiry(Request $request, $paymentId)
    {
        Helper::LogPayment("Begin Inquiry MIDTRANS!". json_encode($request->getContent()));
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // CHECK PAYMENT AVAILABLE
        $clientTransactionDb = ClientTransaction::with('midtrans')
            ->where('payment_id',$paymentId)
            ->first();

            if (!$clientTransactionDb){
            $response->errorMsg = 'Client Transaction Not Found';
            return $response;
        }

        $midSnap = $clientTransactionDb->midtrans;
        if (!$midSnap){
            $response->errorMsg = 'Invalid Payment ID and Method';
            return $response;
        }

        // create QR Image
        $data = new \stdClass();
        $data->payment_id = $clientTransactionDb->payment_id;
        $data->transaction_id = $clientTransactionDb->transaction_id;
        $data->status = $clientTransactionDb->status;
        $data->url = null;
        $data->expired_datetime = null;

        Helper::LogPayment("End Inquiry Midtrans ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create Emoney Card
     * @param Request $request
     * @param $clientTransactionId
     * @return \stdClass
     */
    private function createEmoneyCard(Request $request,$clientTransactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        Helper::LogPayment("Begin Create Emoney Card");

        //required param list
        $required = ['data_emoney'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errorMsg = $message;
            return $response;
        }

        Helper::LogPayment("Create Emoney DB");
        $bank = "MANDIRI";
        $emoneyCard = EMoneyCard::createEmoneyTransaction($request,$clientTransactionId, $bank);
        Helper::LogPayment("Response ".json_encode($emoneyCard));

        if (!$emoneyCard->isSuccess){
            Helper::LogPayment("Failed Emoney Card $emoneyCard->errorMsg");
            $response->errorMsg = $emoneyCard->errorMsg;
            return $response;
        }
        $emoneyId = $emoneyCard->emoneyId;
        $emoneyDb = EMoneyCard::find($emoneyId);

        // create QR Image
        $data = new \stdClass();
        $data->payment_id = $emoneyDb->clientTransaction->payment_id;
        $data->transaction_id = $emoneyDb->clientTransaction->transaction_id;
        $data->status = $emoneyDb->clientTransaction->status;
        $data->url = "";
        $data->expired_datetime = "";

        // update transaction
        $clientTransactionDB = ClientTransaction::find($clientTransactionId);
        $clientTransactionDB->payment_channel_code = $request->input('method_code');
        $clientTransactionDB->save();

        Helper::LogPayment("End Create Emoney Card ".json_encode($data));

        $response->data = $data;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * PAID E-Money
     * @param Request $request
     * @param $clientTransactionId
     * @return \stdClass
     */
    private function paidEmoneyCard(Request $request, $clientTransactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        Helper:LogPayment("Begin PAID Emoney Card");

        //required param list
        $required = ['bank','card_number','reader_id','merchant_id','amount','last_balance'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errorMsg = $message;
            return $response;
        }

        $paidAmount = $request->input('amount');

        Helper::LogPayment("PAID Emoney Card");
        $paidCard = EMoneyCard::paidEmoney($request,$clientTransactionId);
        if (!$paidCard->isSuccess){
            $response->errorMsg = $paidCard->errorMsg;
            return $response;
        }
        Helper::LogPayment("Update Client Transaction ID $clientTransactionId to PAID");
        // update transactionDB
        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        $clientTransactionDb->status = 'PAID';
        $clientTransactionDb->total_payment = $paidAmount;
        $clientTransactionDb->last_payment = $paidAmount;
        $clientTransactionDb->save();

        // insert history
        ClientTransactionHistory::insertHistory($clientTransactionId,'PAID','PAID',$paidAmount);

        $emoneyId = $paidCard->emoneyId;
        $emoneyDb = EMoneyCard::find($emoneyId);

        // create QR Image
        $data = new \stdClass();
        $data->payment_id = $emoneyDb->clientTransaction->payment_id;
        $data->transaction_id = $emoneyDb->clientTransaction->transaction_id;
        $data->status = $emoneyDb->clientTransaction->status;
        $data->url = "";
        $data->expired_datetime = "";

        Helper::LogPayment("Finish PAID Emoney Card");

        $response->isSuccess = true;
        $response->data = $data;
        return $response;
    }

    private function createBillOttopay(Request $request, $clientTransactionId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        Helper::LogPayment("Begin Create OttoPay");

        //required param list
        $required = ['amount', 'customer_name', 'customer_phone', 'customer_email' ,'billing_type'];

        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }

        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errorMsg = $message;
            return $response;
        }

        $client = self::getClient($request->input('token'));

        // Process transaction to DB OttoPay and logging
        $amount = $request->input('amount');
        Helper::LogPayment("Create QR for OttoPay");
        $ottoPay = OttopayTransactions::createTransaction($request,$clientTransactionId, $client);
        Helper::LogPayment("Response ".json_encode($ottoPay));

        if (!$ottoPay->isSuccess) {
            Helper::LogPayment("Failed Create QR OttoPay $ottoPay->errorMsg");
            $response->errorMsg = $ottoPay->errorMsg;
            return $response;
        }
        
        // Get Data OttoPay Transactions
        $ottoPayDb = OttopayTransactions::find($ottoPay->ottoPayId);

        // Response dan url QR Image
        $data = new \stdClass();
        $data->payment_id = $ottoPayDb->clientTransaction->payment_id;
        $data->transaction_id = $ottoPayDb->clientTransaction->transaction_id;
        $data->status = $ottoPayDb->status_payment;
        $data->url = $ottoPay->url;

        // update transaction
        $clientTransactionDB = ClientTransaction::find($clientTransactionId);
        $clientTransactionDB->payment_channel_code = $request->input('method_code');
        $clientTransactionDB->save();
 
        Helper::LogPayment("End Create QR for OttoPay ".json_encode($data));
 
        $response->isSuccess = true;
        $response->data = $data;
        return $response;
    }

    public function tcashBillPayment(Request $request, $clientTransactionId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        Helper::LogPayment("Begin Create tcash");
        $methodCode = $request->input('method_code');
        
        $required = [];
        if ($methodCode != 'TCASH-WEB') {
            //required param list
            $required = ['amount', 'customer_name', 'customer_phone', 'customer_email' ,'billing_type'];
        }

        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }

        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errorMsg = $message;
            return $response;
        }

        // Process transaction to DB OttoPay and logging
        $amount = $request->input('amount');
        Helper::LogPayment("Create QR for tcash");
        $tCash = TcashTransactions::createTransaction($request,$clientTransactionId);
        Helper::LogPayment("Response ".json_encode($tCash));

        if (!$tCash->isSuccess) {
            Helper::LogPayment("Failed Create QR tcash $tCash->errorMsg");
            $response->errorMsg = $tCash->errorMsg;
            return $response;
        }
        
        // Get Data tCash Transactions
        $tcashDb = TcashTransactions::find($tCash->tcashId);

        if ($request->input('method_code') == 'TCASH-WEB') {
            $url = url('webview/tcash')."/$tCash->data";

            $data = new \stdClass();
            $data->url = $url;
            $data->datetime_expired = $tcashDb->date_expired;
            $response->data = $data;
            Helper::LogPayment("End Create Tcash Web Checkout transaction ".json_encode($data));

        } else {
            // Response dan url QR Image
            $data = new \stdClass();
            $data->payment_id = $tcashDb->clientTransaction->payment_id;
            $data->transaction_id = $tcashDb->clientTransaction->transaction_id;
            $data->status = $tcashDb->status;
            $data->url = $tCash->url;
            $response->data = $data;
            Helper::LogPayment("End Create QR for tcash ".json_encode($data));

        }


        // update transaction
        $clientTransactionDB = ClientTransaction::find($clientTransactionId);
        $clientTransactionDB->payment_channel_code = $request->input('method_code');
        $clientTransactionDB->save();

        $response->isSuccess = true;
        return $response;
    }

    public function midtransTrx(Request $request, $clientTransactionId, $method)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        Helper::LogPayment("Begin Create midtrans-gopay");

        $required = [];
        if ($method == 'gopay') {
            $required = ['amount', 'customer_name', 'customer_phone', 'customer_email' ,'billing_type'];
        }

        // get all param
        $input = $request->except('token');
        $methodCode = $request->input('method_code');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }

        // if there is missing parameter
        if (!empty($paramFailed)) {
            $message = "Missing Parameter : " . implode(', ', $paramFailed);
            $response->errorMsg = $message;
            return $response;
        }

        // Process transaction to DB Midtrans Transactions and logging
        $amount = $request->input('amount');
        Helper::LogPayment("Create Midtrans Transactions". $method);
        $midTrans = MidtransTransactions::createTransaction($request,$clientTransactionId);
        Helper::LogPayment("Response ".json_encode($midTrans));

        if (!$midTrans->isSuccess) {
            Helper::LogPayment("Failed Create QR midTrans $midTrans->errorMsg");
            $response->errorMsg = $midTrans->errorMsg;
            return $response;
        }
        
        // Get Data midTrans Transactions
        $midtranshDb = MidtransTransactions::find($midTrans->midtransId);

        // Response dan url QR Image
        $data = new \stdClass();
        $data->payment_id = $midtranshDb->clientTransaction->payment_id;
        $data->transaction_id = $midtranshDb->clientTransaction->transaction_id;
        $data->status = $midtranshDb->status;
        $data->url = $midTrans->url;
        $response->data = $data;
        Helper::LogPayment("End Create QR for gopay ".json_encode($data));

        // update transaction
        $clientTransactionDB = ClientTransaction::find($clientTransactionId);
        $clientTransactionDB->payment_channel_code = $request->input('method_code');
        $clientTransactionDB->save();

        $response->isSuccess = true;
        return $response;
    }

    private static function getClient($token)
    {
        $data = CompanyAccessToken::with('masterClient')->where('token', $token)->first();
        return $data->masterClient->id;
    }

    public function retryPushCallback(Request $request)
    {
        $code = 500;
        $message = null;
        $response = [];
        $payment_type = $request->input('payment_type');
        $payment_id = $request->input('payment_id');
        
        $paymentDb = new ClientTransaction;
        $repush = $paymentDb->repushCallbackClient($payment_id, $payment_type);
        if ($repush->isSuccess) {
            $code = 200;
            $response = $repush;
        } else {
            $response = $repush;
        }
        return ApiHelper::buildResponse($code,$message,$response);
    }

}
