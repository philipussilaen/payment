<?php

namespace App\Http\Models\apiV1;

use App\Http\Helper\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientTransaction extends Model
{
    use SoftDeletes;
    protected $table = 'client_transactions';

    /**
     * Create Transaction
     * @param Request $request
     * @param  null $locationId
     * @return \stdClass
     */
    public static function createTransaction(Request $request,$locationId = null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;

        $companyAccessToken = $request->input('token');
        $methodCode = $request->input('method_code');
        $transactionId = $request->input('transaction_id');
        $merchantName = $request->input('merchant_name',null);
        $customerName = $request->input('customer_name');
        $customerEmail = $request->input('customer_email',null);
        $customerPhone = $request->input('customer_phone',null);
        $description = $request->input('description',null);
        $amount = $request->input('amount',0);
        $paymentType = $request->input('billing_type','fixed');
        $status = 'CREATED';
        $totalAmount = 0;
        $transactionItems = [];

        Helper::LogPayment("Create Transaction With Param ".json_encode($request->input()));

        // check transactionId
        $clientTransactionDb = self::where('transaction_id',$transactionId)->orderBy('created_at','desc')->first();
        if ($clientTransactionDb){
            $lastStatus = $clientTransactionDb->status;
            $response->errorMsg = 'Transaction Id Already Exist And Status '.$lastStatus;
            return $response;
        }

        // get company access token id
        $companyAccessTokenDb = CompanyAccessToken::where('token',$companyAccessToken)->first();
        $companyAccessId = $companyAccessTokenDb->id;
        // generate payment id
        $randomString = Helper::generateRandomString(5);
        $nowDateTime = date('ymdhis');
        $paymentId = "$methodCode$nowDateTime$randomString";

        Helper::LogPayment("Payment Id ".$paymentId);

        // get payment channel
        $paymentChannelDb = PaymentChannel::where('code',$methodCode)->first();
        if (!$paymentChannelDb){
            $response->errorMsg = 'Invalid Payment Channel';
            return $response;
        }
        // get customer payment charges
        $paymentChargesDb = PaymentCharge::where('payment_channels_id',$paymentChannelDb->id)
            ->where('payment_type',$paymentType)
            ->where('charges_type','adders')
            ->get();
        $channelAdminFee = $paymentChannelDb->admin_fee;
        $customerAdminFee = 0;

        foreach ($paymentChargesDb as $item) {
            $chargeType = $item->type;
            $chargeValue = $item->value;
            $chargeName = $item->name;
            $chargeAmount = 0;
            if ($chargeType == 'fixed') $chargeAmount = $chargeValue;
            elseif ($chargeType == 'percent') {
                $chargeAmount = $amount * $chargeValue / 100;
            }
            $customerAdminFee = $chargeAmount;
        }

        // create default items from transaction
        $tmp = new \stdClass();
        $tmp->description = substr($description,0,150);
        $tmp->sub_payment_id = $paymentId;
        $tmp->amount = $amount + $customerAdminFee;
        $tmp->status = $status;
        $tmp->channel_admin_fee = $channelAdminFee;
        $tmp->customer_admin_fee = $customerAdminFee;
        $transactionItems[] = $tmp;
        $totalAmount = $amount + $customerAdminFee;

        // create transaction db
        $clientTransactionDb = new self();
        $clientTransactionDb->company_access_tokens_id = $companyAccessId;
        $clientTransactionDb->payment_channels_id = $paymentChannelDb->id;
        $clientTransactionDb->payment_id = $paymentId;
        $clientTransactionDb->transaction_id = $transactionId;
        $clientTransactionDb->client_name = $merchantName;
        $clientTransactionDb->customer_name = $customerName;
        $clientTransactionDb->customer_email = $customerEmail;
        $clientTransactionDb->customer_phone = $customerPhone;
        $clientTransactionDb->description = substr($description,0,255);
        $clientTransactionDb->total_amount = $totalAmount;
        $clientTransactionDb->status = $status;
        if (!empty($locationId)) $clientTransactionDb->location_id = $locationId;
        $clientTransactionDb->save();

        $clientTransactionId = $clientTransactionDb->id;
        // insert transaction detail
        Helper::LogPayment("Transaction Items : ".json_encode($transactionItems));
        ClientTransactionDetail::insertTransactionDetail($clientTransactionId,$transactionItems);

        // insert history
        Helper::LogPayment("Transaction History : $status, $totalAmount");
        ClientTransactionHistory::insertHistory($clientTransactionId,$status,$totalAmount);

        $response->isSuccess = true;
        $response->transactionId = $clientTransactionId;
        return $response;
    }

    public function repushCallbackClient($paymentId, $payment_type)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        if ($payment_type == 'open') {
            $paymentDb = ClientTransactionDetail::with('clientRepushCallback')->where('sub_payment_id', $paymentId)->first();
        } else {
            $paymentDb = ClientTransaction::with('clientRepushCallback')->where('payment_id', $paymentId)->first();
        }
        
        if ($paymentDb) {
            $repush = $paymentDb->clientRepushCallback;
            
            if ($repush->retry > 1 && $repush->status == "FAILED") {
                $repush->retry = 1;
                $repush->save();
                $response->isSuccess = true;
                $response->message = "Retry push to client Success";
                return $response;
            } else {
                $response->message = "Data Transaksi $paymentId already Success";
                return $response;
            }
        } else {
            $response->message = "Data Transaction $paymentId doesn't exist";
            return $response;
        }
    }

    /*Relationship*/
    public function clientTransactionDetails(){
        return $this->hasMany(ClientTransactionDetail::class,'client_transactions_id','id');
    }

    public function clientTransactionHistories(){
        return $this->hasMany(ClientTransactionHistory::class,'client_transactions_id','id');
    }

    public function BNIVirtualAccount(){
        return $this->hasOne(BNIVirtualAccount::class,'client_transactions_id','id');
    }

    public function BNIYap(){
        return $this->hasOne(BNIYap::class,'client_transactions_id','id');
    }

    public function OttoPay(){
        return $this->hasOne(OttopayTransactions::class,'client_transactions_id','id');
    }

    public function tCash(){
        return $this->hasOne(TcashTransactions::class,'client_transactions_id','id');
    }

    public function midtrans(){
        return $this->hasOne(MidtransTransactions::class,'client_transactions_id','id');
    }

    public function mandiriEmoney(){
        return $this->hasOne(EMoneyCard::class,'client_transactions_id','id');
    }

    public function doku(){
        return $this->hasOne(DokuTransaction::class,'client_transactions_id','id');
    }

    public function molpay(){
        return $this->hasOne(MolPayTransaction::class,'client_transactions_id','id');
    }

    public function location(){
        return $this->belongsTo(Location::class,'location_id','id');
    }

    public function clientRepushCallback(){
        return $this->belongsTo(ClientPushCallback::class,'id','client_transactions_id');
    }
}
