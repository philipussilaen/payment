<?php

namespace App\Http\Models\apiV1;

use App\Jobs\NotificationSendSMS;
use Illuminate\Database\Eloquent\Model;

class NotificationSMS extends Model
{
    // set tables
    protected $table = "notification_sms";

    private $varModule = "";
    private $varVendor = "global";
    private $varTo = "08563052300";
    private $varSms = "";
    private $varStatus = 0;

    /**
     * Create SMS Notification Queue
     * @param string $module
     * @param string $vendor
     * @param string $to
     * @param string $sms
     * @param int $status
     * @return \stdClass
     */
    public function createSMS($module = "",$vendor="",$to="08563052300",$sms = "", $status = 0){
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // set variable
        $this->varModule = $module;
        $this->varVendor = $vendor;
        $this->varTo = $to;
        $this->varSms = $sms;
        $this->varStatus = 0;

        // insert to DB
        DB::beginTransaction();

        $notifDb = new self();
        $notifDb->module = $this->varModule;
        $notifDb->vendor = $this->varVendor;
        $notifDb->to = $this->varTo;
        $notifDb->sms = $this->varSms;
        $notifDb->status = $this->varStatus;

        $notifDb->save();

        // logging
        $this->log();

        DB::commit();

        $response->isSuccess = true;
        dispatch(new NotificationSendSMS($notifDb));
        return $response;
    }
}
