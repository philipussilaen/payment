<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyAccessToken extends Model
{
    protected $table = 'company_access_tokens';
    use SoftDeletes;

    public static function getPaymentChannels($companyAccessToken=null,$param=[]){
        $methodCode = empty($param['method_code']) ? null : $param['method_code'];
        $vendorCode = empty($param['vendor_code']) ? null : $param['vendor_code'];

        $paymentChannelsDb = self::join('token_channel_privileges','company_access_tokens.id','=','token_channel_privileges.company_access_tokens_id')
            ->join('payment_channels','token_channel_privileges.payment_channels_id','=','payment_channels.id')
            ->join('payment_vendors','payment_vendors.id','=','payment_channels.payment_vendors_id')
            ->when($companyAccessToken,function($query) use($companyAccessToken){
                $query->where('company_access_tokens.token',$companyAccessToken);
            })->when($methodCode,function($query) use($methodCode){
                if (is_array($methodCode)) $query->whereIn('payment_channels.code',$methodCode);
                else $query->where('payment_channels.code',$methodCode);
            })->when($vendorCode,function ($query) use($vendorCode){
                if (is_array($vendorCode)) $query->whereIn('payment_vendors.code',$vendorCode);
                else $query->where('payment_vendors.code',$vendorCode);
            })->select('payment_channels.*','token_channel_privileges.status as privileges_status','payment_vendors.status as vendor_status')
            ->get();
        return $paymentChannelsDb;
    }

    /*Relationship*/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company(){
        return $this->belongsTo(Company::class,'id','companies_id');
    }

    public function apiModules(){
        return $this->belongsToMany(ApiModule::class,'token_privileges','id','company_access_tokens_id');
    }

    public function paymentChannels(){
        return $this->belongsToMany(PaymentChannel::class,'token_channel_privileges','id','company_access_tokens_id')->withPivot('status');
    }

    public function masterClient(){
        return $this->belongsTo(MasterClient::class,'id','access_token_id');
    }
}
