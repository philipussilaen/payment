<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class BNIVirtualAccountHistory extends Model
{
    protected $table = 'bni_virtual_account_histories';

    /**
     * Create New History
     * @param $BNIVirtualId
     * @param $param
     */
    public static function createHistory($BNIVirtualId,$param){
        $amount = empty($param['amount']) ? 0 : $param['amount'];
        $customerName = empty($param['customerName']) ? '' : $param['customerName'];
        $datetimeExpired = empty($param['datetimeExpired']) ? date('Y-m-d H:i:s') : $param['datetimeExpired'];
        $description = empty($param['description']) ? 'Create Virtual Account' : $param['description'];
        $paymentAmount = empty($param['paymentAmount']) ? 0 : $param['paymentAmount'];
        $cumulativePayment = empty($param['cumulativeAmount']) ? null : $param['cumulativeAmount'];
        $paymentNtb = empty($param['paymentNtb']) ? null : $param['paymentNtb'];
        $datetimePayment = empty($param['dateTimePayment']) ? null : $param['dateTimePayment'];

        $data = new self();
        $data->bni_virtual_accounts_id = $BNIVirtualId;
        $data->amount = $amount;
        $data->customer_name = $customerName;
        $data->datetime_expired = date('Y-m-d H:i:s',strtotime($datetimeExpired));
        $data->description = substr($description,0,255);
        $data->payment_amount = $paymentAmount;
        $data->cumulative_payment_amount = $cumulativePayment;
        $data->payment_ntb = $paymentNtb;
        $data->datetime_payment = $datetimePayment;
        $data->save();

        return;
    }

    /*Relationship*/
    public function BNIVirtualAccount(){
        return $this->belongsTo(BNIVirtualAccount::class,'bni_virtual_accounts_id','id');
    }
}
