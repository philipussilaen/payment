<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class MasterClient extends Model
{
    protected $table = 'master_client';
}
