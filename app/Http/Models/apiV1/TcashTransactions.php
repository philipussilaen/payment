<?php

namespace App\Http\Models\apiV1;

use Carbon\Carbon;
use App\Http\Helper\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Models\apiV1\ClientTransaction;
use LaravelQRCode\Facades\QRCode;
use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\TcashAPI;


class TcashTransactions extends Model
{
    protected $table = 'tcash_transactions';

    public static function createTransaction(Request $request, $clientTransactionId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->tcashId = null;
        $response->url = null;

        // Logging to File
        Helper::LogPayment("Begin Create Transaction",'tcash');
        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        
        // checking availability ID Transaction
        if (!$clientTransactionDb){
            $response->erroMsg = 'Invalid Transaction';
            return $response;
        }

        $paymentId = $clientTransactionDb->payment_id;
        $transactionAmount = $clientTransactionDb->total_amount;
        $locationId = $clientTransactionDb->location_id;

        $locationDb = Location::find($locationId);
        if ($locationDb){
            $locationId = $locationDb->location_id;
        }

        $paymentChannel = PaymentChannel::where('code', $request->method_code)->first();

        // save to DB
        $tcashDb = new self();
        $tcashDb->client_transactions_id = $clientTransactionId;
        $tcashDb->transaction_amount = $transactionAmount;
        $tcashDb->status = 'Pending';
        $tcashDb->method_code = $request->method_code;
        $tcashDb->payment_channel = $paymentChannel->id;
        $tcashDb->customer_name = $request->customer_name;
        $tcashDb->billing_type = $request->billing_type;      
        $tcashDb->date_expired   = Carbon::now()->addMinutes(2)->toDateTimeString();
        $tcashDb->save();
        Helper::LogPayment("Success Save DB",'tcash');

        if ($request->method_code == 'TCASH') {
            // save image QR
            $fileName = $paymentId.".png";
            $path = public_path()."/img/payment/tcash/$fileName";
            QRCode::text('TWALLET|O|popbox|'.$paymentId)->setSize(7)->setOutfile($path)->png();    
            $url = url("img/payment/tcash/$fileName");
            $response->url = $url;
        } else {
            $tcashApi = new TcashAPI();
            $tcashWeb = $tcashApi->getUrlCheckout($request, $paymentId, $tcashDb->id);
            $resTcash = $tcashWeb->data;
          
            if ($tcashWeb->isSuccess){
                // update for param inquiry
                $tcashDb = TcashTransactions::find($tcashDb->id);
                $tcashDb->param_inquiry  = json_encode($resTcash['post']);   
                $tcashDb->save();

                $response->data = $resTcash['token'];
            } else {
                $response->errorMsg = $tcashWeb->errorMsg;
                return $response;
            }
        }
      
        $response->isSuccess = true;
        $response->tcashId = $tcashDb->id;
        Helper::LogPayment("Finish Create Transaction",'tcash');
        return $response;        
    }

    public static function inquiry(Request $request, $paymentId)
    {
        Helper::LogPayment("Begin Inquiry tcash!");
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = null;

        // CHECK PAYMENT AVAILABLE
        $clientTransactionDb = ClientTransaction::with('tCash')
            ->where('payment_id',$paymentId)
            ->first();

        if (!$clientTransactionDb->tCash) {
            return $response;
        } else {
            $dataReq = [
                "terminal" => $request->input('terminal', null),
                "acc_no" => $request->input('acc_no', null),
                "merchant" => $request->input('merchant', null),
                "trx_type" => $request->input('trx_type', null),
                "msisdn" => $request->input('msisdn', null),
                "pwd" => bcrypt($request->input('pwd', null)),
                "trx_date" => $request->input('trx_date', null),
                "msg" => $request->input('msg', null),
            ];
            $tcashDb = TcashTransactions::find($clientTransactionDb->tCash->id);
            $tcashDb->param_inquiry    = json_encode($dataReq);
            $tcashDb->trx_type         = $request->trx_type;
            $tcashDb->save();

            $data = [
                'bill_name' => $clientTransactionDb->tCash->customer_name,
                'bill_amount' => $clientTransactionDb->tCash->transaction_amount,
                'bill_ref' => $clientTransactionDb->payment_id,
            ];
        }

        Helper::LogPayment("End Create QR for tcash ".json_encode($data));
        
        $response->isSuccess = true;
        $response->data = $data;
        return $response;
    }

    public static function payment(Request $request, $paymentId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $dataReq = [
            "terminal" => $request->input('terminal', null),
            "acc_no" => $request->input('acc_no', null),
            "merchant" => $request->input('merchant', null),
            "trx_type" => $request->input('trx_type', null),
            "msisdn" => $request->input('msisdn', null),
            "pwd" => bcrypt($request->input('pwd', null)),
            "trx_date" => $request->input('trx_date', null),
            "msg" => $request->input('msg', null),
            "amount" => $request->input('amount', null),
            "bill_ref" => $request->input('bill_ref', null),
            "trx_id" => $request->input('trx_id', null),
        ];

        $trx_amount = $request->input('amount', null);

        // get client Transaction DB
        $clientTransactionDb = ClientTransaction::with('tCash')
            ->where('payment_id',$paymentId)
            ->first();

        if (!$clientTransactionDb){
            $response->errorMsg = 'Client Transaction Not Found';
            return $response;
        }

        $tcashDb = $clientTransactionDb->tCash;
        if (!$tcashDb){
            $response->errorMsg = 'Invalid Payment ID and Method';
            return $response;
        }

        $paymentStatus = $clientTransactionDb->status;
        $clientTransactionId = $clientTransactionDb->id;
        Helper::LogPayment("Inquiry $paymentId $paymentStatus",'tcash','inquiry');

        if ($paymentStatus == 'CREATED') {
            Helper::LogPayment('Begin Inquiry to tcash','tcash','inquiry');

            if ($trx_amount) {
                $encodedData = json_encode($dataReq);
                Helper::LogPayment("Update DB with result $encodedData",'tcash','inquiry');
                
                // Time Convert Manual
                if (isset($request->trx_date)) {
                    $datePay = str_replace(" ", "-", substr($request->transactionTime, 0, 10));
                    $timePay = substr($request->transactionTime, -8);
                    $dateTimePay = date('Y-m-d H:i:s',strtotime( $datePay." ".$timePay ));
                }

                DB::beginTransaction();

                $tcashDb = TcashTransactions::find($tcashDb->id);
                $tcashDb->trx_id    = isset($request->trx_id) ? $request->trx_id : null;
                $tcashDb->trx_type  = isset($request->trx_type) ? $request->trx_type : null;
                $tcashDb->last_payment_amount = isset($request->amount) ? $request->amount : null;
                $tcashDb->param_payment       = $encodedData;
                $tcashDb->payment_date        = isset($request->trx_date) ? $dateTimePay : null;
                $tcashDb->status              = "Success";
                $tcashDb->ref_number          = isset($request->bill_ref) ? $request->bill_ref : null;
                $tcashDb->save();

                 // update transactionDB
                 $paymentAmount = isset($request->amount) ? $request->amount : null;
                 $clientTransactionDb = ClientTransaction::find($clientTransactionDb->id);
                 $clientTransactionDb->status = 'PAID';
                 $clientTransactionDb->total_payment = $paymentAmount;
                 $clientTransactionDb->last_payment = $paymentAmount;
                 $clientTransactionDb->save();
                 Helper::LogPayment("Update Transaction $clientTransactionId to 'PAID",'tcash','inquiry');
 
                 // insert history
                 ClientTransactionHistory::insertHistory($clientTransactionId,'PAID','PAID',$paymentAmount);
                 Helper::LogPayment("Insert History Transaction 'PAID",'tcash','inquiry');
 
                 // update client transaction detail to paid
                 $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
                 if ($clientTransactionDetailDb){
                     $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
                     $clientTransactionDetailDb->paid_amount = $paymentAmount;
                     $clientTransactionDetailDb->status = 'PAID';
                     $clientTransactionDetailDb->save();
                 }
                
                DB::commit();

                $data = [
                    'trx_id' => $request->trx_id,
                    'bill_ref' => $clientTransactionDb->payment_id,
                    'notif' => 'Pembayaran Berhasil, Silahkan Konfirmasi pada Loker'
                ];

            } else {
                $errorMsg = $inquiry->errorMsg;
                Helper::LogPayment('Inquiry Failed. '.$errorMsg,'tcash','inquiry');
            }       
        }
        
        $response->isSuccess = true;
        $response->data = $data;
        return $response;
    }

    public static function paymentReversal(Request $request, $paymentId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $dataReq = [
            "terminal" => $request->input('terminal', null),
            "acc_no" => $request->input('acc_no', null),
            "merchant" => $request->input('merchant', null),
            "trx_type" => $request->input('trx_type', null),
            "msisdn" => $request->input('msisdn', null),
            "pwd" => bcrypt($request->input('pwd', null)),
            "trx_date" => $request->input('trx_date', null),
            "msg" => $request->input('msg', null),
            "amount" => $request->input('amount', null),
            "bill_ref" => $request->input('bill_ref', null),
            "trx_id" => $request->input('trx_id', null),
        ];

        $trx_amount = $request->input('amount', null);

        // get client Transaction DB
        $clientTransactionDb = ClientTransaction::with('tCash')
            ->where('payment_id',$paymentId)
            ->first();

        if (!$clientTransactionDb){
            $response->errorMsg = 'Client Transaction Not Found';
            return $response;
        }

        $tcashDb = $clientTransactionDb->tCash;
        if (!$tcashDb){
            $response->errorMsg = 'Invalid Payment ID and Method';
            return $response;
        }

        $paymentStatus = $clientTransactionDb->status;
        $clientTransactionId = $clientTransactionDb->id;
        Helper::LogPayment("Inquiry $paymentId $paymentStatus",'tcash','inquiry');

        if ($paymentStatus == 'CREATED') {
            Helper::LogPayment('Begin Inquiry to tcash','tcash','inquiry');

            if ($trx_amount) {
                $encodedData = json_encode($dataReq);
                Helper::LogPayment("Update DB with result $encodedData",'tcash','inquiry');
                
                // Time Convert Manual
                if (isset($request->trx_date)) {
                    $datePay = str_replace(" ", "-", substr($request->transactionTime, 0, 10));
                    $timePay = substr($request->transactionTime, -8);
                    $dateTimePay = date('Y-m-d H:i:s',strtotime( $datePay." ".$timePay ));
                }

                DB::beginTransaction();

                $tcashDb = TcashTransactions::find($tcashDb->id);
                $tcashDb->trx_id    = isset($request->trx_id) ? $request->trx_id : null;
                $tcashDb->trx_type  = isset($request->trx_type) ? $request->trx_type : null;
                $tcashDb->last_payment_amount = isset($request->amount) ? $request->amount : null;
                $tcashDb->param_payment       = $encodedData;
                $tcashDb->payment_date        = isset($request->trx_date) ? $dateTimePay : null;
                $tcashDb->status              = "failed";
                $tcashDb->ref_number          = isset($request->bill_ref) ? $request->bill_ref : null;
                $tcashDb->save();

                 // update transactionDB
                 $paymentAmount = isset($request->amount) ? $request->amount : null;
                 $clientTransactionDb = ClientTransaction::find($clientTransactionDb->id);
                 $clientTransactionDb->status = 'FAILED';
                 $clientTransactionDb->total_payment = $paymentAmount;
                 $clientTransactionDb->last_payment = $paymentAmount;
                 $clientTransactionDb->save();
                 Helper::LogPayment("Update Transaction $clientTransactionId to 'FAILED",'tcash','inquiry');
 
                 // insert history
                 ClientTransactionHistory::insertHistory($clientTransactionId,'FAILED','FAILED',$paymentAmount);
                 Helper::LogPayment("Insert History Transaction 'FAILED",'tcash','inquiry');
 
                 // update client transaction detail to paid
                 $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
                 if ($clientTransactionDetailDb){
                     $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
                     $clientTransactionDetailDb->paid_amount = $paymentAmount;
                     $clientTransactionDetailDb->status = 'FAILED';
                     $clientTransactionDetailDb->save();
                 }
                
                DB::commit();

                $data = [
                    'trx_id' => $request->trx_id,
                    'bill_ref' => $clientTransactionDb->payment_id,
                    'notif' => 'Reversal Success'
                ];

            } else {
                $errorMsg = $inquiry->errorMsg;
                Helper::LogPayment('Inquiry Failed. '.$errorMsg,'tcash','inquiry');
            }       
        }
        
        $response->isSuccess = true;
        $response->data = $data;
        return $response;
    }

    public function updateRefNumber($refNum, $tcashId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $tcashDb = TcashTransactions::find($tcashId);
        $tcashDb->ref_number = $refNum;
        
        if ($tcashDb->save()){
            $tcashDb->save();
            $response->isSuccess = true;
            return $response;
        }

        return $response;
    }

     /* Relation to table ClientTransactions */
    public function clientTransaction(){
        return $this->belongsTo(\App\Http\Models\apiV1\ClientTransaction::class, 'client_transactions_id','id');
    }
}
