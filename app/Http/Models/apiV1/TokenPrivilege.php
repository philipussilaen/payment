<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class TokenPrivilege extends Model
{
    protected $table = 'token_privileges';

    /**
     * Check Token, Module and Privileges
     * @param $token
     * @param $module
     * @return \stdClass
     */
    public static function checkPrivileges($token,$module){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->moduleId = null;
        $response->companyTokenId = null;

        // get company access token db
        $companyAccessTokenDb = CompanyAccessToken::where('token',$token)->first();
        if (!$companyAccessTokenDb){
            $response->errorMsg = 'Token Not Found';
            return $response;
        }
        // get module DB
        $apiModuleDb = ApiModule::where('name',$module)->first();
        if (!$apiModuleDb){
            $response->errorMsg = 'Module Not Found';
            return $response;
        }
        // check privileges
        $tokenPrivilegesDb = self::where('company_access_tokens_id',$companyAccessTokenDb->id)
            ->where('api_modules_id',$apiModuleDb->id)
            ->first();
        if (!$tokenPrivilegesDb){
            $response->errorMsg = 'Unauthorized';
            return $response;
        }
        $response->isSuccess = true;
        $response->moduleId = $apiModuleDb->id;
        $response->companyTokenId = $companyAccessTokenDb->id;
        return $response;
    }
}
