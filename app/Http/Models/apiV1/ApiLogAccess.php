<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ApiLogAccess extends Model
{
    protected $table = 'api_logs_access';

    /**
     * insert log
     * @param $modulesId
     * @param $companyTokenId
     * @param $sessionId
     * @param Request $request
     * @return mixed
     */
    public static function insertLog($modulesId,$companyTokenId,$sessionId,Request $request){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $apiLogAccessDb = new self();
        $apiLogAccessDb->api_modules_id = $modulesId;
        $apiLogAccessDb->company_access_tokens_id = $companyTokenId;
        $apiLogAccessDb->session_id = $sessionId;
        $apiLogAccessDb->request = json_encode($request->input());
        $apiLogAccessDb->latency = 0;
        $apiLogAccessDb->response = '';
        $apiLogAccessDb->save();

        return $apiLogAccessDb->id;
    }

    /**
     * Update Log Access
     * @param $logId
     * @param $latency
     * @param $response
     */
    public static function updateLog($logId,$latency,$response){
        $apiLogAccessDb = self::find($logId);
        $apiLogAccessDb->response = $response->content();
        $apiLogAccessDb->latency = $latency;
        $apiLogAccessDb->save();
        return;
    }
}
