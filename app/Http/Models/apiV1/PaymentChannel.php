<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class PaymentChannel extends Model
{
    protected $table = 'payment_channels';

    /*Relationship*/
    public function paymentVendor(){
        return $this->belongsTo(PaymentVendor::class,'payment_vendors_id','id');
    }

    public function clientTransaction(){
        return $this->hasMany(ClientTransaction::class,'payment_channels_id','id');
    }

    public function companyAccessTokens(){
        return $this->belongsToMany(CompanyAccessToken::class,'token_channel_privileges','id','payment_channels_id')->withPivot('status');
    }
}
