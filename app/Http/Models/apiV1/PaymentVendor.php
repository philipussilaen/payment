<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class PaymentVendor extends Model
{
    protected $table = 'payment_vendors';

    /*Relationship*/
    public function paymentChannels(){
        return $this->hasMany(PaymentChannel::class,'payment_vendors_id','id');
    }
}
