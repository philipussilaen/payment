<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class CurlResponse extends Model
{
    protected $table = 'curl_responses';
}
