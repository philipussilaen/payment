<?php

namespace App\Http\Models\apiV1;

use Carbon\Carbon;
use App\Http\Helper\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class EMoneyCard extends Model
{
    protected $table = 'emoney_cards';

    /**
     * Create EMoney Transaction
     * @param Request $request
     * @param $clientTransactionId
     * @return \stdClass
     */
    public static function createEmoneyTransaction(Request $request, $clientTransactionId, $bank){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->emoneyId = null;

        Helper::LogPayment("Begin Create Transaction",'emoney');
        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        if (!$clientTransactionDb){
            $response->erroMsg = 'Invalid Transaction';
            return $response;
        }
        Helper::LogGeneral($clientTransactionDb->payment_id." - ".$request->getContent(), 'mandiri_settlement');

        $paymentId = $clientTransactionDb->payment_id;
        $transactionAmount = $clientTransactionDb->total_amount;

        $reqParse = json_decode($request->getContent());
        $dataEmoney = json_decode($reqParse->data_emoney);

        if (property_exists($dataEmoney, 'settle_code')) {
            $settle_code = $dataEmoney->settle_code;
        } else {
            $settle_code = null;
        }

        $cardNumber = $dataEmoney->card_no;
        $readerId = $dataEmoney->terminal_id;
        $merchantId = $dataEmoney->raw;
        $lastBalance = $dataEmoney->last_balance;
        $paidAmount = $reqParse->amount;
        $showDate =  $dataEmoney->show_date;
        $lockerLocation = $reqParse->location_name;

        Helper::LogPayment("Bank $bank, number $cardNumber, readerId $readerId, merchant $merchantId","emoney");

        // save to DB
        $cardDb = new self();
        $cardDb->client_transactions_id = $clientTransactionId;
        $cardDb->bank = $bank;
        $cardDb->card_number = $cardNumber;
        $cardDb->reader_id = $readerId;
        $cardDb->settle_code = $settle_code;
        $cardDb->merchant_id = $merchantId;
        $cardDb->transaction_amount = $transactionAmount;
        $cardDb->paid_amount = $paidAmount;
        $cardDb->last_balance = $lastBalance;
        $cardDb->locker_location = $lockerLocation;
        $cardDb->show_date = $showDate;
        $cardDb->status = 'PAID';
        $cardDb->save();

        $response->isSuccess = true;
        $response->emoneyId = $cardDb->id;
        return $response;
    }

    /**
     * Insert emoney
     * @param Request $request
     * @param $emoneyId
     * @return \stdClass
     */
    public static function paidEmoney(Request $request,$clientTransactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->emoneyId = null;

        Helper::LogPayment("Begin PAID Emoney Transaction",'emoney');

        $clientTransactionDb = ClientTransaction::find($clientTransactionId);
        if (!$clientTransactionDb){
            $response->erroMsg = 'Invalid Transaction';
            return $response;
        }
        $cardDb = self::where('client_transactions_id',$clientTransactionId)->first();
        if (!$cardDb){
            $response->errorMsg = 'Invalid Card';
            return $response;
        }

        $bank = $request->input('bank');
        $cardNumber = $request->input('card_number');
        $readerId = $request->input('reader_id');
        $merchantId = $request->input('merchant_id');
        $paidAmount = $request->input('amount');
        $lastBalance = $request->input('last_balance');

        Helper::LogPayment("Bank $bank, number $cardNumber, readerId $readerId, merchant $merchantId","emoney");

        // save to DB
        $cardDb = self::find($cardDb->id);
        $cardDb->bank = $bank;
        $cardDb->card_number = $cardNumber;
        $cardDb->reader_id = $readerId;
        $cardDb->merchant_id = $merchantId;
        $cardDb->paid_amount = $paidAmount;
        $cardDb->last_balance = $lastBalance;
        $cardDb->status = 'PAID';
        $cardDb->save();

        $response->isSuccess = true;
        $response->emoneyId = $cardDb->id;
        Helper::LogPayment("End PAID Emoney Transaction",'emoney');
        return $response;
    }


    /*Relationship*/
    public function clientTransaction(){
        return $this->belongsTo(ClientTransaction::class,'client_transactions_id','id');
    }
}
