<?php

namespace App\Http\Models\apiV1;

use App\Http\Helper\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApiSession extends Model
{
    protected $table = 'api_sessions';
    use SoftDeletes;

    /**
     * Check Session ID status based on Session Id and Token
     * @param $token
     * @param $sessionId
     * @return \stdClass
     */
    public static function checkSession($token,$sessionId){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        //check if token and session id exist
        $apiSessionDb = self::where('session_id',$sessionId)
            ->whereHas('companyAccessToken',function($query) use($token){
                $query->where('token',$token);
            })->first();
        if (!$apiSessionDb){
            $response->errorMsg = 'Session Id Not Valid';
            return $response;
        }

        // check if expired
        $nowTime = time(); #current time in the number of seconds since the Unix Epoch
        $sessionExpired = $apiSessionDb->expired;
        if ($nowTime > $sessionExpired){
            $response->errorMsg = 'Session Expired. Please Create Session';
            return $response;
        }

        // update session last_activity
        $expiredDuration = 900; #15 minutes in second
        $apiSessionDb = self::find($apiSessionDb->id);
        $apiSessionDb->last_activity = $nowTime;
        $apiSessionDb->expired = $nowTime+$expiredDuration;
        $apiSessionDb->save();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create Session
     * @param $companyAccessTokenId
     * @param null $ipAddress
     * @return \stdClass
     */
    public static function createSession($companyAccessTokenId,$ipAddress=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->sessionId = null;

        $sessionId = Helper::generateRandomString(25,'AlphaNumMix');

        $apiSessionDb = new self();
        $apiSessionDb->company_access_tokens_id = $companyAccessTokenId;
        $apiSessionDb->session_id = $sessionId;
        $apiSessionDb->ip_address = $ipAddress;

        $nowTime = time();
        $expiredDuration = 900; #15 minutes in second
        $expiredTime = $nowTime + $expiredDuration;

        $apiSessionDb->last_activity = $nowTime;
        $apiSessionDb->expired = $expiredTime;
        $apiSessionDb->save();

        $response->isSuccess = true;
        $response->sessionId = $sessionId;
        return $response;
    }

    /*Relationship*/
    public function companyAccessToken(){
        return $this->belongsTo(CompanyAccessToken::class,'company_access_tokens_id','id');
    }
}
