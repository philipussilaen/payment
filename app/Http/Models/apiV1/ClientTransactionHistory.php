<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientTransactionHistory extends Model
{
    protected $table = 'client_transaction_histories';
    use SoftDeletes;

    /**
     * Insert Transaction History
     * @param $clientTransactionId
     * @param string $status
     * @param null $remarks
     */
    public static function insertHistory($clientTransactionId,$status='',$remarks=null, $amount=0){
        if (!empty($remarks)) substr($remarks,0,255);

        $data = new self();
        $data->client_transactions_id = $clientTransactionId;
        $data->status = $status;
        $data->remarks = $remarks;
        $data->amount = $amount;
        $data->save();

        return;
    }

    /*Relationship*/
    public function clientTransaction(){
        return $this->belongsTo(ClientTransaction::class,'client_transactions_id','id');
    }
}
