<?php

namespace App\Http\Models\apiV1;

use Illuminate\Database\Eloquent\Model;

class ClientPushCallback extends Model
{
    protected $table = 'client_push_callbacks';
}
