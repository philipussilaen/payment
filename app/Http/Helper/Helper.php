<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 26/07/2017
 * Time: 12.22
 */

namespace App\Http\Helper;

class Helper
{
    /**
     * Generate Random String
     * Type : AlphaNumUpper, AlphaNumLower, AlphaNumMix
     * @param string $type
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10,$type='AlphaNumUpper') {

        $typeList = [
            'AlphaNumUpper' => '23456789ABCDEFGHJKLMNPQRSTUVWXYZ',
            'AlphaNumLower' => '23456789abcdefghjkmnpqrstuvxyz',
            'AlphaNumMix' => '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz',
            'Numeric' => '0123456789'
        ];

        $selectedType = $typeList['AlphaNumUpper'];
        if (array_key_exists($type,$typeList)){
            $selectedType = $typeList[$type];
        }

        $characters = $selectedType;
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Log Payment
     * @param null $sessionId
     * @param string $folderName
     * @param string $filename
     * @param $message
     */
    public static function LogPayment($message,$folderName='',$filename='payment',$sessionId=null){
        if (empty($sessionId)){
            $sessionId = session('session_id');
        }
        $message = "$sessionId > $message\n";
        $f = fopen(storage_path().'/payment/'.$folderName.'/'.$filename.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $message");
        fclose($f);
    }

    /**
     * General Log on storage/logs directory
     * @param $message
     * @param string $folderName
     * @param string $filename
     * @param null $sessionId
     */
    public static function LogGeneral($message,$folderName='',$filename='',$sessionId=null){
        if (empty($sessionId)){
            $sessionId = session('session_id');
        }
        
        $path = storage_path().'/logs/'.$folderName;
        if (!is_dir($path)){
            mkdir($path);
        }

        $message = "$sessionId > $message\n";
        $f = fopen(storage_path().'/logs/'.$folderName.'/'.$filename.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $message");
        fclose($f);
    }
}