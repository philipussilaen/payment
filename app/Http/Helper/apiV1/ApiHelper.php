<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 26/07/2017
 * Time: 12.04
 */

namespace App\Http\Helper\apiV1;


class ApiHelper
{
    /**
     * Build response for API
     * @param int $code
     * @param string $message
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public static function buildResponse($code=400,$message='Failed Response',$data=array()){
        $response = new \stdClass();
        $response->code = $code;
        $response->message = $message;

        $result = new \stdClass();
        $result->response = $response;
        if (!is_array($data)) $result->data = [$data];
        else $result->data = $data;

        return response()->json($result);
    }

    /**
     * Build hex to base64 for auth API
     * @param int $merchantId
     * @param string $serverKey
     * @param string $timestamp
     * @return $hashing
     */
    public static function createSignature($merchantId, $timestamp, $serverKey)
    {      
        $hash   = strtoupper( hash_hmac("sha256", "".$merchantId.":".$timestamp.":".$serverKey."", "".$serverKey."" ) );
        $result = base64_encode(pack('H*', $hash));
        return $result;
    }

    public static function signatureStringBuilder($requestBody, $timestamp, $serverKey)
    {
       // formula HMAC-256(uppercase(trim(requestBody)):timestamp, Server Key)
        $hash = hash_hmac("sha256", preg_replace("/\"/","", strtoupper(str_replace('_', '', $requestBody))).":".$timestamp, $serverKey);
        $result = base64_encode(pack('H*', $hash));
        return $result;      
    }
}