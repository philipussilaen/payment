<?php 

namespace App\Http\Libraries;

use App\Http\Models\apiV1\CurlResponse;
use App\Http\Helper\apiV1\ApiHelper;
use App\Http\Models\apiV1\MidtransTransactions;
use App\Http\Models\apiV1\ClientTransactions;

class MidtransAPI 
{

    private $midtransURL = null;
    private $snapURL = null;
    private $serverKey   = null;
    private $snapCurl   = null;

    public function __construct()
    {
        $this->midtransURL = env('MIDTRANS_API_URL', "https://api.sandbox.midtrans.com/v2");
        $this->snapURL     = env('MIDTRANS_SNAP_URL', "https://app.sandbox.midtrans.com/snap/v1/transactions");
        $this->serverKey   = env('MIDTRANS_SERVER_KEY', "SB-Mid-server-2vf3_UatOqZ0wp25sPKG_PqM"); 
        $this->snapCurl    = env('MIDTRANS_SNAP_CURL', null);
    }

    public function cUrl($params = [])
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->responseCode = "200";
        $response->errorMsg = null;
        
        if($params['payment_type'] != 'snap') {
            $url = $this->midtransURL."/charge";
        } else {
            $url = $this->snapURL;
        }

        $header[] = "Content-Type: application/json";
        $header[] = "Accept: application/json";
        $header[] = "Authorization: Basic ". base64_encode($this->serverKey.":");

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url); // Create Payments
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        $resApi = curl_exec($ch);
        curl_close($ch);

        $resultApi = json_decode($resApi);
        if (!empty($resultApi->status_code)) {
            if ( $resultApi->status_code != "201") {
                $response->responseCode = "401";
                $response->errorMsg = ($resultApi->status_message);
                $this->logApiFile("Error Message when Create Billing ".$resApi);
                return $response;
            }
        }

        $this->saveResponse($url,$params,$resApi);
        $message = $resApi;
        $this->logApiFile("Create Success ".$message);

        $response->isSuccess = true;
        $response->data = $resultApi;
        return $response;
    }

    public function createGopayTrx($request, $paymentId)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->responseCode = "200";
        $response->errorMsg = null;

        // Start to Get QRCODE Id From midtrans
         $postData = [
            "payment_type" => "gopay",
            "transaction_details" => [
                "order_id" => $paymentId,
                "gross_amount" => $request->input('amount')
            ],
            "item_details" => [
                [
                "price" => $request->input('amount'),
                "quantity" => 1,
                "name" => $request->input('description')
                ]
            ],
        ];

        $curl = $this->cUrl($postData);        
        
        $response->isSuccess = true;
        $response->data = $curl->data;
        return $response;
    }

    public function createSnapTrx($request, $paymentId, $type)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->responseCode = "200";
        $response->errorMsg = null;

        // Start to webview snap midtrans
         $postData = [
            "payment_type" => "snap",
            "transaction_details" => [
                "order_id" => $paymentId,
                "gross_amount" => $request->input('amount')
            ],
            "enabled_payments" => ["gopay"]
        ];

        $curl = $this->cUrl($postData);  
        if ($type == 'locker') {
            $curl = $this->curlSnap($curl);
        }

        $response->isSuccess = true;
        $response->data = $curl->data;
        return $response;
    }


    private function curlSnap($curl)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->responseCode = "200";
        $response->errorMsg = null;

        $url = explode('/', $curl->data->redirect_url);
        $url = $this->snapURL."/". $url[6]."/pay";

        $header[] = "Content-Type: application/json";
        $header[] = "Referer: ".$this->snapCurl."/".$url[6];
        $header[] = "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";
        $header[] = "X-Source: vtweb";
        $header[] = "X-Source-Version: 1.37.0";

        $params = [
            "payment_type" => "gopay"
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        $res = curl_exec($ch);
        curl_close($ch);
        
        $response->data = json_decode($res);
        return $response;
    }

    /**
     * Save Response from APIs
     * @param $url
     * @param $param
     * @param $response
     */
    private function saveResponse($url,$param,$response){
        $data = new CurlResponse();
        $data->api_url = $url;
        $data->api_send_data = json_encode($param);
        $data->api_response = $response;
        $data->save();
        return;
    }

    /**
     * Log
     * @param $message
     * @param string $type
     */
    private function logApiFile($message,$type='api'){
        $message = " $message\n";
        $f = fopen(storage_path().'/payment/midtrans/gopay/'.$type.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $message");
        fclose($f);
    }
}




?>