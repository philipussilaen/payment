<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 15/08/2017
 * Time: 10.49
 */

namespace App\Http\Libraries;


use App\Http\Models\apiV1\MolPayChannel;

class MolPay
{
    public $molpayUrl;
    public $verificationKey;
    public $merchantId;

    public function __construct()
    {
        $this->molpayUrl = env('MOLPAY_URL','https://www.onlinepayment.com.my/MOLPay/pay/test7776/');
        $this->verificationKey = env('MOLPAY_KEY','f5236a94bbf8c2d92b544e6ab94d3aed');
        $this->merchantId = env('MOLPAY_MERCHANT','test7776');
    }

    public function createURL($param){
        $response = new \stdClass();
        $response->url = null;
        $response->vCode = null;

        $amount = $param['amount'];
        $orderId = $param['orderid'];
        $channel = $param['channel'];
        $baseUrl = $this->molpayUrl;
        $merchantId = $this->merchantId;
        $verificationKey = $this->verificationKey;

        $channelDb = MolPayChannel::where('code',$channel)->first();
        if (!$channelDb){
            return $response;
        }

        $filenameChannel = $channelDb->filename;

        // generate vCode
        $vCode = md5($amount.$merchantId.$orderId.$verificationKey);
        $tmpParam = '';
        foreach ($param as $key => $item) {
            $tmpParam.="&$key=$item";
        }
        $finalUrl = $baseUrl."$filenameChannel?vcode=$vCode&merchant_id=$merchantId".$tmpParam;
        $response->url = $finalUrl;
        $response->vCode = $vCode;
        return $response;
    }
}