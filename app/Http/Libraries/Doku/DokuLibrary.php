<?php
/**
 * Doku's Global Function
 */
namespace App\Http\Libraries\Doku;
class Doku_Library
{
    public static function doCreateWords($data)
    {
        $dokuInitiate = new Doku_Initiate();
        if (!empty($data['device_id'])) {
            if (!empty($data['pairing_code'])) {
                return sha1($data['amount'] . $dokuInitiate->mallId . $dokuInitiate->sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id']);
            } else {
                return sha1($data['amount'] . $dokuInitiate->mallId . $dokuInitiate->sharedKey . $data['invoice'] . $data['currency'] . $data['device_id']);
            }
        } else if (!empty($data['pairing_code'])) {
            return sha1($data['amount'] . $dokuInitiate->mallId . $dokuInitiate->sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code']);
        } else if (!empty($data['currency'])) {
            return sha1($data['amount'] . $dokuInitiate->mallId . $dokuInitiate->sharedKey . $data['invoice'] . $data['currency']);
        } else {
            return sha1($data['amount'] . $dokuInitiate->mallId . $dokuInitiate->sharedKey . $data['invoice']);
        }
    }
    public static function doCreateWordsRaw($data)
    {
        $dokuInitiate = new Doku_Initiate();
        if (!empty($data['device_id'])) {
            if (!empty($data['pairing_code'])) {
                return $data['amount'] . $dokuInitiate->mallId . $dokuInitiate->sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id'];
            } else {
                return $data['amount'] . $dokuInitiate->mallId . $dokuInitiate->sharedKey . $data['invoice'] . $data['currency'] . $data['device_id'];
            }
        } else if (!empty($data['pairing_code'])) {
            return $data['amount'] . $dokuInitiate->mallId . $dokuInitiate->sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'];
        } else if (!empty($data['currency'])) {
            return $data['amount'] . $dokuInitiate->mallId . $dokuInitiate->sharedKey . $data['invoice'] . $data['currency'];
        } else {
            return $data['amount'] . $dokuInitiate->mallId . $dokuInitiate->sharedKey . $data['invoice'];
        }
    }
    public static function formatBasket($data)
    {
        $parseBasket = '';
        if (is_array($data)) {
            foreach ($data as $basket) {
                $parseBasket = $parseBasket . $basket['name'] . ',' . $basket['amount'] . ',' . $basket['quantity'] . ',' . $basket['subtotal'] . ';';
            }
        } else if (is_object($data)) {
            foreach ($data as $basket) {
                $parseBasket = $parseBasket . $basket->name . ',' . $basket->amount . ',' . $basket->quantity . ',' . $basket->subtotal . ';';
            }
        } else {
            $parseBasket = $data;
        }
        return $parseBasket;
    }
}
