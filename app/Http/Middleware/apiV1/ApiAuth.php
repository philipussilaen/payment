<?php

namespace App\Http\Middleware\apiV1;

use App\Http\Models\apiV1\ApiLogAccess;
use App\Http\Models\apiV1\ApiSession;
use App\Http\Models\apiV1\Company;
use App\Http\Models\apiV1\TokenPrivilege;
use Closure;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // start time request
        $startTime = microtime(true);
        // create default response
        $status = 200;
        $message = null;
        $response = new \stdClass();
        $response->code = $status;
        $response->message = $message;

        // check content type
        // if (!$request->hasHeader('Content-Type') || $request->header('Content-Type')!='application/json'){
        if (!$request->hasHeader('Content-Type') || (strpos($request->header('Content-Type'), 'application/json') == true)) {
            $status = 400;
            $message = 'Invalid Content-Type';
            $date = date('Y.m.d');
                $time = date('H:i:s');
                $msg  = ">> $time $message - ". $request->getContent();
                $f = fopen(storage_path().'/logs/apiauth'.$date.'.log','a');
                fwrite($f,$msg);
                fclose($f);

            return $this->falseReturn($status,$message,$startTime);
        }

        // check token
        $token = $request->input('token',null);
        if (empty($token)){
            $status = 400;
            $message = 'Missing Token';
            return $this->falseReturn($status,$message,$startTime);
        }

        // ignored for session
        $module = $request->path();
        $ignoredModule = ['api/v1/createSession'];
        // if not in ignored module then check Session
        $sessionId = $request->input('session_id',null);
        if (!in_array($module,$ignoredModule)){
            if (!$sessionId){
                $status = 400;
                $message = 'Missing Session ID';
                return $this->falseReturn($status,$message,$startTime);
            }
            // check session is valid, if not status 403
            $checkSessionDb = ApiSession::checkSession($token,$sessionId);
            if (!$checkSessionDb->isSuccess){
                $status = 403;
                $message = $checkSessionDb->errorMsg;
                return $this->falseReturn($status,$message,$startTime);
            }
        }

        // check token, module, privileges, if not allowed status 401
        $ignoredModule = [];
        if (!in_array($module,$ignoredModule)){
            $checkPrivilegesDb = TokenPrivilege::checkPrivileges($token,$module);
            if (!$checkPrivilegesDb->isSuccess){
                $status = 401;
                $message = $checkPrivilegesDb->errorMsg;
                return $this->falseReturn($status,$message,$startTime);
            }
            // log access
            $moduleId = $checkPrivilegesDb->moduleId;
            $companyTokenId = $checkPrivilegesDb->companyTokenId;
            $logId = ApiLogAccess::insertLog($moduleId,$companyTokenId,$sessionId,$request);
        }
        $response =  $next($request);

        if (!in_array($module,$ignoredModule)){
            $endTime = microtime(true);
            $latency = $endTime- $startTime;
            ApiLogAccess::updateLog($logId,$latency,$response);

            $originalResponse = $response->original;
            if (!isset($originalResponse->response->latency)){
                $originalResponse->response->latency = $latency;
                $originalResponse = json_encode($originalResponse);
                $response->setContent($originalResponse);
            };
        }
        return $response;
    }

    private function falseReturn($status = 400, $message="General Error",$startTime){
        $response = new \stdClass();
        $response->code = $status;
        $response->message = $message;

        $endTime = microtime(true);

        $latency = $endTime- $startTime;

        $result = new \stdClass();
        $response->code = $status;
        $response->message = $message;
        $response->latency = $latency;
        $result->response = $response;
        return response()->json($result);
    }
}
