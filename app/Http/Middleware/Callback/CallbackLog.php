<?php

namespace App\Http\Middleware\Callback;

use Closure;

class CallbackLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uniqueId = uniqid();
        // set start time
        $startTime = microtime(true);
        // get url
        $url = $request->path();
        // get IP
        $ip = $request->ip();
        //get param
        $param = json_encode($request->except('token'));
        $this->log("$uniqueId $ip - $url - $param");

        $response = $next($request);
        // calculate duration
        $endTime = microtime(true);
        $duration = ($endTime - $startTime);

        $logResponse = ($response->content());
        $this->log("$uniqueId $duration $logResponse");
        return $response;
    }

    private function log($msg=''){
        $msg = " $msg\n";
        $f = fopen(storage_path().'/payment/callback'.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $msg");
        fclose($f);
    }
}
