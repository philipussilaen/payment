<?php

namespace App\Console\Commands;

use App\Http\Helper\Helper;
use App\Http\Models\apiV1\BNIYap;
use App\Http\Models\apiV1\ClientTransaction;
use App\Http\Models\apiV1\NotificationSMS;
use App\Jobs\NotificationSendSMS;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class YapInquiry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yap:inquiry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BNI Yap Inquiry Payment Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->log("Begin Cron Inquiry BNI Yap");

        // get BNI YAP Transaction
        $bniYapTransactionDb = BNIYap::with('clientTransaction')
            ->where('status','CREATED')
            ->get();

        $nowTime = time();

        foreach ($bniYapTransactionDb as $item) {
            DB::beginTransaction();
            $paymentId = $item->clientTransaction->payment_id;
            $transactionId = $item->clientTransaction->transaction_id;
            $amount = $item->clientTransaction->total_amount;
            $expiredDateTime = $item->expired_datetime;

            $this->log("Process $paymentId $amount ex: $expiredDateTime");
            // check expired
            $expiredTime = strtotime($expiredDateTime);


            if ($expiredTime < $nowTime){
                $this->log("Expired on $expiredTime < $nowTime");
                // set expired
                $transactionDb = ClientTransaction::find($item->clientTransaction->id);
                $transactionDb->status = 'EXPIRED';
                $transactionDb->save();

                $bniYapDb = BNIYap::find($item->id);
                $bniYapDb->status = 'EXPIRED';
                $bniYapDb->save();

                DB::commit();
                continue;
            }

            // inquiry payment to YAP
            $BniYap = BNIYap::inquiryYap($paymentId);
            if (!$BniYap->isSuccess){
                DB::rollback();
                $errorMsg = $BniYap->errorMsg;
                $this->log("Failed Inquiry with message $errorMsg");
                continue;
            }
            $this->log("Success Inquiry $paymentId");
            DB::commit();

            // create Send SMS
            $transactionDb = ClientTransaction::find($item->clientTransaction->id);
            if ($transactionDb->status == 'PAID'){
                $phone = $transactionDb->customer_phone;
                $locationName = "PopBox";
                $date = date('Y-m-d H:i:s',strtotime($transactionDb->updated_at));
                if (!empty($transactionDb->location)){
                    $locationName .="@ ".$transactionDb->location->location_name;
                }
                $smsMessage = "Pembayaran $transactionId sukses Rp".number_format($amount)." pada $date, di $locationName";

                $notifDb = new NotificationSMS();
                $notifDb->client_transaction_id = $transactionDb->id;
                $notifDb->module = 'YAP-Inquiry';
                $notifDb->vendor = 'global';
                $notifDb->to =$phone;
                $notifDb->sms = $smsMessage;
                $notifDb->status = 0;
                $notifDb->save();

                dispatch(new NotificationSendSMS($notifDb));
            }
        }

        $this->log("End Cron Inquiry BNI Yap");
    }

    private function log($message){
        echo "$message\n";
        Helper::LogPayment($message,'yap','inquiry');
    }
}
