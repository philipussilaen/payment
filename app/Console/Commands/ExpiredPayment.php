<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Http\Helper\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Http\Models\apiV1\OttopayTransactions;
use App\Http\Models\apiV1\ClientTransaction;
use App\Http\Models\apiV1\ClientTransactionDetail;
use App\Http\Models\apiV1\ClientTransactionHistory;

class ExpiredPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expiredPayment:updateStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Payment Status for expired payment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $data = \DB::table('client_transactions')
                ->select('payment_id')
                ->join('ottopay_transactions', 'client_transactions.id', '=', 'ottopay_transactions.client_transactions_id')
                ->where('ottopay_transactions.status_payment', 'Pending')
                ->whereNotNull('ottopay_transactions.billing_id')
                ->where('ottopay_transactions.created_at','<=', Carbon::now()->subDay())
                ->orderBy('ottopay_transactions.id', 'desc')
                ->get();
       
        $paymentId = [];
        foreach ($data as $key => $value) {
            $paymentId[] = $value->payment_id;
        }   
       
        $updateTrx =  $this->updateStatusExpired($paymentId);

        echo "Transaction Berhasil \n";

    }

    private function updateStatusExpired($allPaymentId = [])
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        foreach ($allPaymentId as $key => $paymentId) {

            DB::beginTransaction();        
        
            // $paymentAmount = isset($checkUpdateTrx->gross_amount) ? $checkUpdateTrx->gross_amount : null;
            $clientTransactionDb = ClientTransaction::where('payment_id', $paymentId)->first();
            $clientTransactionId = $clientTransactionDb->id;
            $clientTransactionDb->status = 'FAILED';
            // $clientTransactionDb->total_payment = $paymentAmount;
            // $clientTransactionDb->last_payment = $paymentAmount;
            $clientTransactionDb->save();

            $ottoPayDB = OttopayTransactions::where('client_transactions_id', $clientTransactionDb->id)->first();
            // $ottoPayDB->reference_number    = isset($checkUpdateTrx->referenceNumber) ? $checkUpdateTrx->referenceNumber : null;
            $ottoPayDB->last_payment_amount = null;
            $ottoPayDB->param_response      = null;
            $ottoPayDB->payment_date        = null;
            $ottoPayDB->status_payment      = "Expired";
            $ottoPayDB->save();

            // update transactionDB
            Helper::LogPayment("Update Transaction $clientTransactionId to 'PAID",'ottopay','inquiry');

            // insert history
            ClientTransactionHistory::insertHistory($clientTransactionId,'FAILED','FAILED', 0);
            Helper::LogPayment("Insert History Transaction 'FAILED",'ottopay','inquiry');

            // update client transaction detail to paid
            $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
            if ($clientTransactionDetailDb){
                $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
                // $clientTransactionDetailDb->paid_amount = $paymentAmount;
                $clientTransactionDetailDb->status = 'FAILED';
                $clientTransactionDetailDb->save();
            }

            if (!$clientTransactionDb->save() || !$ottoPayDB->save() || !$clientTransactionDetailDb->save()) {
                DB::rollBack();
                Helper::LogPayment("Gagal Melakukan Transaction dengan payment_id ". $clientTransactionDb->payment_id ,'ottopay','inquiry');
                // return $response;
            }

            DB::commit();
            Helper::LogPayment("Berhasil Melakukan Transaction dengan payment_id ". $clientTransactionDb->payment_id ,'ottopay','inquiry');
        }
    }
}
