<?php

namespace App\Console;

use App\Console\Commands\PermissionChange;
use App\Console\Commands\YapInquiry;
use App\Console\Commands\OttopayCallback;
use App\Console\Commands\ExpiredPayment;
use App\Console\Commands\TcashCheckPayment;
use App\Console\Commands\UploadMandiriSettlement;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\ClientRepushCallback::class,
        \App\Console\Commands\DbaseBackup::class,
        YapInquiry::class,
        PermissionChange::class,
        OttopayCallback::class,
        ExpiredPayment::class,
        TcashCheckPayment::class,
        UploadMandiriSettlement::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $nowDate = date('Y.m.d');
        $location = storage_path()."/logs/cron/";
        $schedule->command('client:repushCallback')->everyMinute()->withoutOverlapping()->appendOutputTo($location."clientRepush.$nowDate.txt");
        $schedule->command('yap:inquiry')->everyMinute()->withoutOverlapping()->appendOutputTo($location."yapInquiry.$nowDate.txt");
        $schedule->command('dbasebackup')->dailyAt('03:00');
        $schedule->command('expiredPayment:updateStatus')->dailyAt('12:00');
        $schedule->command('mandirisettlement:upload')->dailyAt('22:00');
        $schedule->command('ottopay:paymentstatus')->everyFiveMinutes();
        $schedule->command('tcash:paymentstatus')->everyFiveMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
