<?php

namespace App\Jobs;

use App\Http\Helper\Helper;
use App\Http\Models\apiV1\ClientPushCallback;
use App\Http\Models\apiV1\ClientTransaction;
use App\Http\Models\apiV1\ClientTransactionDetail;
use App\Http\Models\apiV1\CompanyAccessToken;
use App\Http\Models\apiV1\CurlResponse;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class SendClientCallback implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    public $timeout = 30;

    protected $clientTransaction;
    protected $uniqueId = null;
    /**
     * Create a new job instance.
     * @param $clientTransaction ClientTransaction
     * @return void
     */
    public function __construct(ClientTransaction $clientTransaction)
    {
        $this->clientTransaction = $clientTransaction;
        $this->uniqueId = uniqid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $clientTransactionDb = $this->clientTransaction;

        $paymentChannel = $clientTransactionDb->payment_channel_code;
        $companyAccessTokenId = $clientTransactionDb->company_access_tokens_id;
        $paymentId = $clientTransactionDb->payment_id;
        $transactionId = $clientTransactionDb->transaction_id;
        $paymentType = $clientTransactionDb->payment_type;
        $status = $clientTransactionDb->status;
        Helper::LogGeneral("Begin Job Push Callback Client $transactionId $paymentId $paymentChannel $paymentType",'client','callback');

        // check type
        $pushStatus = 'FAILED';
        $errorMessage = null;
        if ($paymentType == 'fixed'){
            $pushResponse = $this->pushFixed();
            if (!$pushResponse->isSuccess){
                $errorMessage = is_array($pushResponse->errorMsg) ? json_encode($pushResponse->errorMsg) : $pushResponse->errorMsg;
            } else {
                $pushStatus = 'SUCCESS';
            }
        } elseif ($paymentType == 'open'){
            $pushResponse = $this->pushOpen();
            if (!$pushResponse->isSuccess){
                $errorMessage = is_array($pushResponse->errorMsg) ? json_encode($pushResponse->errorMsg) : $pushResponse->errorMsg;
            } else {
                $pushStatus = 'SUCCESS';
            }
        } else {
            $errorMessage = 'Undefined Payment Type';
        }

        Helper::LogGeneral("End Job Push Callback Client $pushStatus - $errorMessage",'client','callback');
    }

    /**
     * Push Fixed
     * @return \stdClass
     */
    private function pushFixed(){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $clientTransactionDb = $this->clientTransaction;
        $paymentChannel = $clientTransactionDb->payment_channel_code;
        $companyAccessTokenId = $clientTransactionDb->company_access_tokens_id;
        $paymentId = $clientTransactionDb->payment_id;
        $transactionId = $clientTransactionDb->transaction_id;
        $status = $clientTransactionDb->status;
        $paidAmount = $clientTransactionDb->total_payment;

        // get company access token db
        $companyAccessTokenDb = CompanyAccessToken::find($companyAccessTokenId);
        $url = $companyAccessTokenDb->fixed_payment_callback;

        Helper::LogGeneral("Begin Push Fixed $url",'client','callback');

        if (empty($url)){
            $response->errorMsg = 'Empty Fixed Callback URL for '.$companyAccessTokenDb->token;
            return $response;
        }

        // check if there is additional parameter for client
        $additionalParam = $companyAccessTokenDb->additional_parameter_callback;
        $parameter = [];
        if (!empty($additionalParam)){
            $parameter = json_decode($additionalParam,true);
        }

        // create parameter
        $parameter['transaction_id'] = $transactionId;
        $parameter['payment_id'] = $paymentId;
        $parameter['paid_amount'] = $paidAmount;
        $parameter['status'] = $status;
        $parameter['payment_channel_code'] = $paymentChannel;

        // push to client
        $clientResponse = $this->postClient($url,$parameter);
        $pushStatus = 'FAILED';
        $errorMessage = null;
        if (empty($clientResponse)){
            $errorMessage = 'Failed to push';
        }
        if (!isset($clientResponse['is_success'])){
            $errorMessage = 'is_success Not Found';
        } elseif ($clientResponse['is_success']!=true){
            $errorMessage = $clientResponse;
        } else {
            $pushStatus = 'SUCCESS';
        }

        $this->saveCallbackClient($url,$parameter,$clientResponse,$pushStatus,$clientTransactionDb->id,null,$errorMessage);
        if ($pushStatus=='FAILED'){
            $response->errorMsg = $errorMessage;
            Helper::LogGeneral("End Push Fixed ".json_encode($response),'client','callback');
            return $response;
        }
        $response->isSuccess = true;
        Helper::LogGeneral("End Push Fixed ".json_encode($response),'client','callback');
        return $response;
    }

    /**
     * Push Open
     * @return \stdClass
     */
    private function pushOpen(){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $clientTransactionDb = $this->clientTransaction;

        $paymentChannel = $clientTransactionDb->payment_channel_code;
        $companyAccessTokenId = $clientTransactionDb->company_access_tokens_id;
        $paymentId = $clientTransactionDb->payment_id;
        $transactionId = $clientTransactionDb->transaction_id;
        $status = $clientTransactionDb->status;
        $customerAdminFee = 0;

        // get last transaction detail
        $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionDb->id)->orderBy('id','desc')->first();
        $clientTransactionDetailId = $clientTransactionDetailDb->id;
        $paidAmount = $clientTransactionDetailDb->paid_amount;
        $lastPaidAmount = $clientTransactionDetailDb->amount;
        $customerAdminFee = $clientTransactionDetailDb->customer_admin_fee;

        // get company access token db
        $companyAccessTokenDb = CompanyAccessToken::find($companyAccessTokenId);
        $url = $companyAccessTokenDb->open_payment_callback;
        Helper::LogGeneral("Begin Push Fixed $url",'client','callback');

        if (empty($url)){
            $response->errorMsg = 'Empty Fixed Callback URL for '.$companyAccessTokenDb->token;
            return $response;
        }

        // check if there is additional parameter for client
        $parameter = [];
        $additionalParam = $companyAccessTokenDb->additional_parameter_callback;
        if (!empty($additionalParam)){
            $parameter = json_decode($additionalParam,true);
        }

        // create parameter
        $parameter['transaction_id'] = $transactionId;
        $parameter['payment_id'] = $paymentId;
        $parameter['sub_payment_id'] = $clientTransactionDetailDb->sub_payment_id;
        $parameter['paid_amount'] = $paidAmount;
        $parameter['status'] = $status;
        $parameter['payment_channel_code'] = $paymentChannel;
        $parameter['last_paid_amount'] = $lastPaidAmount;
        $parameter['customer_admin_fee'] = $customerAdminFee;

        // push to client
        $clientResponse = $this->postClient($url,$parameter);
        $pushStatus = 'FAILED';
        $errorMessage = null;
        if (empty($clientResponse)){
            $errorMessage = 'Failed to push';
        }

        if (is_object($clientResponse)) $clientResponse = (array)$clientResponse;

        if (!isset($clientResponse['is_success'])){
            $errorMessage = 'is_success Not Found';
        } elseif ($clientResponse['is_success']!=true){
            $errorMessage = $clientResponse;
        } else {
            $pushStatus = 'SUCCESS';
        }

        $this->saveCallbackClient($url,$parameter,$clientResponse,$pushStatus,$clientTransactionDb->id,$clientTransactionDetailId,$errorMessage);
        if ($pushStatus=='FAILED'){
            $response->errorMsg = $errorMessage;
            Helper::LogGeneral("End Push Fixed ".json_encode($response),'client','callback');
            return $response;
        }
        $response->isSuccess = true;
        Helper::LogGeneral("End Push Fixed ".json_encode($response),'client','callback');
        return $response;
    }

    /**
     * Push To Client
     * @param $url
     * @param $param
     * @param $response
     * @param string $status
     * @param $clientTransactionId
     * @param null $clientTransactionDetailId
     * @param null $errorMessage
     */
    private function saveCallbackClient($url,$param,$response,$status='SUCCESS',$clientTransactionId,$clientTransactionDetailId=null,$errorMessage=null){
        $param = json_encode($param);

        // check on table client_push_callbacks
        $checkPush = ClientPushCallback::where('client_transactions_id',$clientTransactionId)
            ->when($clientTransactionDetailId,function($query) use ($clientTransactionDetailId){
                return $query->where('client_transaction_details_id',$clientTransactionDetailId);
            })->first();

        $clientTransactionDb = $this->clientTransaction;
        // get company access token db
        $paymentType = $clientTransactionDb->payment_channel_code;
        $lastPush = date('Y-m-d H:i:s');

        if (!is_string($errorMessage)) $errorMessage = json_encode($errorMessage);
        if (is_array($errorMessage)) $errorMessage = json_encode($errorMessage);

        if (!is_string($response)) $response = json_encode($response);
        if (is_array($response)) $response = json_encode($response);

        if (!is_string($param)) $param = json_encode($param);
        if (is_array($param)) $param = json_encode($param);
        // insert new client
        if (!$checkPush){
            Helper::LogGeneral("Begin Insert new callback client",'client','callback');

            $clientPush = new ClientPushCallback();
            $clientPush->client_transactions_id = $clientTransactionId;
            $clientPush->client_transaction_details_id = $clientTransactionDetailId;
            $clientPush->payment_type = $paymentType;
            $clientPush->status = $status;
            $clientPush->url = $url;
            $clientPush->last_push = $lastPush;
            $clientPush->retry = 1;
            $clientPush->error_message = $errorMessage;
            $clientPush->request = $param;
            $clientPush->response = $response;
            $clientPush->save();
        } else {
            $lastRetry = $checkPush->retry;
            $newRetry = $lastRetry +1;
            Helper::LogGeneral("Begin Update callback client Retry $newRetry",'client','callback');
            $clientPush = ClientPushCallback::find($checkPush->id);
            $clientPush->payment_type = $paymentType;
            $clientPush->status = $status;
            $clientPush->url = $url;
            $clientPush->last_push = $lastPush;
            $clientPush->retry = $newRetry;
            $clientPush->error_message = $errorMessage;
            $clientPush->request = $param;
            $clientPush->response = $response;
            $clientPush->save();
        }
        return;
    }

    /**
     * cUrl Post to client
     * @param $url
     * @param array $param
     * @param $clientTransactionId
     * @param null $clientTransactionDetailId
     * @return mixed
     */
    private function postClient($url,$param = array()){
        $param = json_encode($param);
        Helper::LogGeneral("Begin cUrl $url, $param",'client','callback');

        $curl = curl_init();
        curl_setopt ($curl, CURLOPT_URL,$url);
        curl_setopt ( $curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt ( $curl, CURLOPT_POST, true );
        curl_setopt ( $curl, CURLOPT_POSTFIELDS, $param );
        curl_setopt ( $curl, CURLOPT_COOKIEJAR, "" );
        curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $curl, CURLOPT_ENCODING, "" );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $curl, CURLOPT_AUTOREFERER, true );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
        curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_MAXREDIRS, 10 );
        $result=curl_exec ($curl);
        $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);   //get status code
        $info = curl_getinfo($curl);
        curl_close ($curl);

        $tmp = $result;
        $result = json_decode($result,true);

        $param = json_decode($param);
        if (empty($result)){
            $this->saveResponse($url,$param,$tmp);
        } else $this->saveResponse($url,$param,json_encode($result));

        Helper::LogGeneral("End Push cUrl ",'client','callback');

        return $result;
    }

    /**
     * Save Response from postClient
     * @param $url
     * @param $param
     * @param $response
     */
    private function saveResponse($url,$param,$response){
        $data = new CurlResponse();
        $data->api_url = $url;
        $data->api_send_data = json_encode($param);
        $data->api_response = $response;
        $data->save();
        return;
    }
}
