<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\Helper;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Http\Models\apiV1\ClientTransaction;
use App\Http\Models\apiV1\ClientTransactionDetail;
use App\Http\Models\apiV1\ClientTransactionHistory;
use App\Http\Models\apiV1\OttopayTransactions;
use App\Http\Models\apiV1\CheckUpdateTemp;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class CheckUpdatePayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $checkUpdateTemp;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($checkUpdateTemp)
    {
        $this->checkUpdateTemp = $checkUpdateTemp;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $checkUpdateTrx = $this->checkUpdateTemp;
        $paymentId      = $checkUpdateTrx->order_id; 
        $status         = ($checkUpdateTrx->transaction_status == 'Success') ? 'PAID' : $checkUpdateTrx->transaction_status;

        // Begin Transaction
        DB::beginTransaction();        
    
        $paymentAmount = isset($checkUpdateTrx->gross_amount) ? $checkUpdateTrx->gross_amount : null;
        $clientTransactionDb = ClientTransaction::where('payment_id', $paymentId)->first();
        $clientTransactionId = $clientTransactionDb->id;
        $clientTransactionDb->status = $status;
        $clientTransactionDb->total_payment = $paymentAmount;
        $clientTransactionDb->last_payment = $paymentAmount;
        $clientTransactionDb->save();

        $ottoPayDB = OttopayTransactions::where('client_transactions_id', $clientTransactionDb->id)->first();
        // $ottoPayDB->reference_number    = isset($checkUpdateTrx->referenceNumber) ? $checkUpdateTrx->referenceNumber : null;
        $ottoPayDB->last_payment_amount = isset($checkUpdateTrx->gross_amount) ? $checkUpdateTrx->gross_amount : null;
        $ottoPayDB->param_response      = $checkUpdateTrx->param_req;
        $ottoPayDB->payment_date        = isset($checkUpdateTrx->transaction_time) ? $checkUpdateTrx->transaction_time : null;
        $ottoPayDB->status_payment      = isset($checkUpdateTrx->transaction_status) ? $checkUpdateTrx->transaction_status : null;
        $ottoPayDB->save();

        // update transactionDB
        Helper::LogPayment("Update Transaction $clientTransactionId to $status",'ottopay','inquiry');

        // insert history
        ClientTransactionHistory::insertHistory($clientTransactionId,$status ,$status ,$paymentAmount);
        Helper::LogPayment("Insert History Transaction $status ",'ottopay','inquiry');

        // update client transaction detail to paid
        $clientTransactionDetailDb = ClientTransactionDetail::where('client_transactions_id',$clientTransactionId)->first();
        if ($clientTransactionDetailDb){
            $clientTransactionDetailDb = ClientTransactionDetail::find($clientTransactionDetailDb->id);
            $clientTransactionDetailDb->paid_amount = $paymentAmount;
            $clientTransactionDetailDb->status = $status ;
            $clientTransactionDetailDb->save();
        }

        if (!$clientTransactionDb->save() || !$ottoPayDB->save() || !$clientTransactionDetailDb->save()) {
            DB::rollBack();
            CheckUpdateTemp::where('order_id', $checkUpdateTrx->order_id)->delete();
            Helper::LogPayment("Gagal Melakukan Transaction dengan payment_id ". $clientTransactionDb->payment_id ,'ottopay','inquiry');
            echo "Gagal Melakukan Transaction dengan payment_id ". $clientTransactionDb->payment_id;
        }

        DB::commit();
        CheckUpdateTemp::where('order_id', $checkUpdateTrx->order_id)->delete();
        Helper::LogPayment("Berhasil Melakukan Transaction dengan payment_id ". $clientTransactionDb->payment_id ,'ottopay','inquiry');
        echo "Transaction Berhasil \n";
    }
}
