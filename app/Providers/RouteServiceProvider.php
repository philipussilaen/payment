<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiV1Routes();
        $this->mapCallbackRoutes();
        $this->mapWebRoutes();
        $this->mapWebViewRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "apiV1" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiV1Routes()
    {
        Route::prefix('api/v1')
            ->middleware(['sessions','api','apiV1'])
            ->namespace('App\Http\Controllers\apiV1')
            ->group(base_path('routes/apiV1.php'));
    }

    protected function mapCallbackRoutes(){
        Route::prefix('api/callback')
            ->middleware(['sessions','api','callback'])
            ->namespace('App\Http\Controllers')
            ->group(base_path('routes/callback.php'));
    }

    protected function mapWebViewRoutes(){
        Route::prefix('webview')
            ->middleware(['sessions','webView'])
            ->namespace('App\Http\Controllers')
            ->group(base_path('routes/webview.php'));
    }
}
