<?php
return [
    'production' => [
        'host'      => env('MANDIRI_SETTLEMENT_HOST'),
        'port'      => env('MANDIRI_SETTLEMENT_PORT'),
        'username'  => env('MANDIRI_SETTLEMENT_USERNAME'),
        'password'  => env('MANDIRI_SETTLEMENT_PASSWORD'),
        'key'       => '',
        'keytext'   => '',
        'keyphrase' => '',
        'agent'     => '',
        'timeout'   => 10,
    ],
    'staging' => [
        'host'      => env('MANDIRI_SETTLEMENT_HOST'),
        'port'      => env('MANDIRI_SETTLEMENT_PORT'),
        'username'  => env('MANDIRI_SETTLEMENT_USERNAME'),
        'password'  => env('MANDIRI_SETTLEMENT_PASSWORD'),
        'timeout'   => 10,
    ],
    'test' => [
        'host'      => '3.3.3.3:3',
        'username'  => 'test',
        'password'  => 'testpass',
        'timeout'   => 10,
    ],
];