<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 02/08/2017
 * Time: 09.00
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('bni','CallbackController@BniCallback');
Route::post('molpay/return','CallbackController@molPayReturn');
Route::post('molpay/notify','CallbackController@');
Route::post('molpay/callback','CallbackController@molPayCallback');
Route::post('doku/notify','CallbackController@dokuNotify');
Route::post('doku/redirect','CallbackController@dokuRedirect');
Route::any('doku/inquiry','CallbackController@dokuOpenNotify');
Route::any('ottopay/notify','CallbackController@ottoPayNotify');
Route::any('tcash/billpayment','CallbackController@tcashBillPayment');
Route::any('tcash/webcheckout/success','CallbackController@tcashSuccess');
Route::any('tcash/webcheckout/failed','CallbackController@tcashFailed');
Route::any('midtrans/gopay/notification','CallbackController@midtransNotify');