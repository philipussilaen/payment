<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Payment PopBox Asia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/bootstrap-4.min.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
    <style>
    html,
    body {
        height: 100%;
        font-family: 'Didact Gothic', sans-serif;
    }

    .text-center {
        margin: 0 !important;
    }

    #img-payment {
        style="width: 30%;"
    }
    

    @media only screen and (max-width: 600px) {
        #img-payment {
            width: 90%;
        }
    }
    </style>
</head>
<body>
    <div class="h-100 row align-items-center">
        <div class="col">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @if ($payChannel == 'TCASH') 

                            @if ($status == 'success') 
                                <img class="mx-auto d-block" src="{{ asset('img/payment/logo-popbox.png') }}" alt="popbox">
                                <br>
                                <img class="mx-auto d-block" id="img-payment" src="{{ asset('img/payment/icon/success-opsi.png') }}" alt="success">
                                    <br />
                                    <p class="text-center" style="font-size: 21px"><b>Yay! Payment Complete</b></p>
                                    <p class="text-center">Your payment has been successfully processed.</p>
                                    <p class="text-center">Press back button to close this page.</p>
                            @else 
                                <img class="mx-auto d-block" src="{{ asset('img/payment/logo-popbox.png') }}" alt="popbox">
                                <br>
                                <img class="mx-auto d-block" src="{{ asset('img/payment/icon/failed-opsi.png') }}" id="img-payment" alt="success">
                                    <br />
                                    <p class="text-center" style="font-size: 21px"><b>Opps! Payment Failed</b></p>
                                    <p class="text-center">Sorry, your payment failed.</p>
                                    <p class="text-center">No charges were made.</p>
                                    <p class="text-center">Press back button to close this page.</p>
                            @endif                

                        @elseif ($payChannel == 'DOKU')

                            @if ($status=="success" || $status == 'ok' || $status=='SUCCESS')
                                <img class="mx-auto d-block" src="{{ asset('img/payment/logo-popbox.png') }}" alt="popbox">
                                <br>
                                <img class="mx-auto d-block" id="img-payment" src="{{ asset('img/payment/icon/success-opsi.png') }}" alt="success">
                                    <br />
                                    <p class="text-center" style="font-size: 21px"><b>Yay! Payment Complete</b></p>
                                    <p class="text-center">Your payment has been successfully processed.</p>
                                    <p class="text-center">Press back button to close this page.</p>
                            @else
                                <img class="mx-auto d-block" src="{{ asset('img/payment/logo-popbox.png') }}" alt="popbox">
                                <br>
                                <img class="mx-auto d-block" src="{{ asset('img/payment/icon/failed-opsi.png') }}" id="img-payment" alt="success">
                                    <br />
                                    <p class="text-center" style="font-size: 21px"><b>Opps! Payment Failed</b></p>
                                    <p class="text-center">Sorry, your payment failed.</p>
                                    <p class="text-center">No charges were made.</p>
                                    <p class="text-center">Press back button to close this page.</p>
                            @endif

                        @endif
                    </div>
                </div>
            </div>
           
        </div>
    </div>

</body>
</html>