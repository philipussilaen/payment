<html> 
	<head>
		<title></title>
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">		
		<link href="{{ asset('css/bootstrap-theme.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/payment.css') }}" rel="stylesheet">
		
		<script src="{{ asset('js/jquery.min.js') }}"></script>
		<script src="{{ asset('js/bootstrap.js') }}"></script>
	</head>
	<body>
		<div id="head">			
		</div>
		<div id="content">			
				<div class="row">					
					<div class="col-md-12 col-sm-12 col-xs-12 failed-img">
						<center>
							@if ($status=="success" || $status == 'ok' || $status=='SUCCESS')
								<img src="{{URL::asset('img/payment/succeed.png')}}"><br/>
								<div class="word-failed">Thank you</div>
								<br>
								<div class="word-desc">Your Payment is Success. Transaction being Processed<br/>
									Press back button to close this page.
								</div>
							@else
								<img src="{{URL::asset('img/payment/failed.png')}}"><br/>
								<div class="word-failed">Payment Failed</div>
								<br>
								<div class="word-desc">We are sorry your payment failed. Please Contact your bank / PopBox for further assistance<br/>
									Press back button to close this page.
								</div>
							@endif
						</center>						
					</div>
				</div>
			
		</div>
		<div id="footer"></div>
	</body>
</html>
