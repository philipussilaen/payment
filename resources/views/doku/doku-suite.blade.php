<html>
    <head>
        <script src="{{ asset('js/jquery.min.js') }}"></script>
    </head>
    <body>
        <form action="{{ $dokuUrl }}" method="post" id="form_doku">
            @foreach($formParameter as $key => $value)
                @if(!empty($value))
                    <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                @endif
            @endforeach
        </form>
    </body>
</html>
<script type="text/javascript">
    $("#form_doku").submit();
</script>