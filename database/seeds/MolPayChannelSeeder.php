<?php

use Illuminate\Database\Seeder;

class MolPayChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('molpay_channel_lists')
            ->insert([
                [
                    'code' => 'credit', 'name' => 'Visa / MasterCard',  'filename' => 'index.php',
                    'range_amount' => '1', 'downtime' => null, 'information' => 'credit/debit/prepaid card',
                    'status' => 'OPEN'
                ],
                [
                    'code' => 'jompay', 'name' => 'JomPay', 'filename' => 'jompay.php',
                    'range_amount' => '1', 'downtime' => null, 'information' => 'direct Internet banking',
                    'status' => 'OPEN'
                ],
                [
                    'code' => 'fpx', 'name' => 'MyClear FPX', 'filename' => 'fpx.php',
                    'range_amount' => '1', 'downtime' => '12am - 1am', 'information' => 'indirect internet banking',
                    'status' => 'OPEN'
                ],
                [
                    'code' => 'maybank2u', 'name' => 'Maybank2u', 'filename' => 'maybank2u.php',
                    'range_amount' => '1', 'downtime' => '12:15am - 1am', 'information' => 'direct internet banking',
                    'status' => 'OPEN'
                ],
                [
                    'code' => 'cimb', 'name' => 'CIMB Clicks', 'filename' => 'cimb.php',
                    'range_amount' => '1', 'downtime' => '11:45pm - 12:45am', 'information' => 'direct internet banking / CIMB credit card',
                    'status' => 'OPEN'
                ],
                [
                    'code' => 'BIMB', 'name' => 'Bank Islam', 'filename' => 'BIMB.php',
                    'range_amount' => '1', 'downtime' => '12:15am - 1am', 'information' => 'direct internet banking',
                    'status' => 'OPEN'
                ],
                [
                    'code' => 'amb', 'name' => 'Am Online', 'filename' => 'amb.php',
                    'range_amount' => '1', 'downtime' => '12am - 6am', 'information' => 'direct internet banking',
                    'status' => 'OPEN'
                ],
                [
                    'code' => 'MOLPoints', 'name' => 'MOLPoints', 'filename' => 'MOLPoints.php',
                    'range_amount' => '1', 'downtime' => null, 'information' => 'e-wallet',
                    'status' => 'OPEN'
                ],
                [
                    'code' => 'MOLWallet', 'name' => 'MOLWallet', 'filename' => 'MOLWallet.php',
                    'range_amount' => '1', 'downtime' => null, 'information' => 'e-wallet',
                    'status' => 'OPEN'
                ],
                [
                    'code' => 'webcash', 'name' => 'Webcash', 'filename' => 'webcash.php ',
                    'range_amount' => '1', 'downtime' => null, 'information' => 'e-wallet',
                    'status' => 'OPEN'
                ],
                [
                    'code' => '7-eleven', 'name' => 'cash', 'filename' => 'cash.php',
                    'range_amount' => '1', 'downtime' => null, 'information' => 'cash',
                    'status' => 'OPEN'
                ],
                [
                    'code' => 'epay', 'name' => 'epay at Petronas', 'filename' => 'epay.php',
                    'range_amount' => '1', 'downtime' => null, 'information' => 'cash',
                    'status' => 'OPEN'
                ]
            ]);
    }
}
