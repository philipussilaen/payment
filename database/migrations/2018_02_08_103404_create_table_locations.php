<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('location_id',64)->comment('Location ID');
            $table->string('location_type',32)->nullable()->comment('Location Type: locker,agent,personal');
            $table->string('location_name',256)->comment('Location name');
            $table->text('address')->nullable()->comment('Location Address');
            $table->timestamps();
        });

        Schema::table('client_transactions', function (Blueprint $table) {
            $table->integer('location_id',false,true)->nullable()->after('status')->comment('Location ID if available');

            $table->foreign('location_id')->references('id')->on('locations');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');

        Schema::table('client_transactions', function (Blueprint $table) {
            $table->dropColumn('location_id');
        });
    }
}
