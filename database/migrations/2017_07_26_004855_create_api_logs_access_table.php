<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiLogsAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_logs_access', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('api_modules_id');
            $table->unsignedInteger('company_access_tokens_id');
            $table->string('session_id',255)->nullable();
            $table->text('request');
            $table->text('response')->nullable();
            $table->float('latency')->nullable();
            $table->timestamps();

            $table->foreign('api_modules_id')->references('id')->on('api_modules');
            $table->foreign('company_access_tokens_id')->references('id')->on('company_access_tokens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_logs_access');
    }
}
