<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMolpayTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('molpay_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_transactions_id');
            $table->string('transaction_id',100)->nullable();
            $table->string('domain',255)->nullable();
            $table->float('amount')->nullable();
            $table->string('currency',20)->nullable();
            $table->string('channel',100)->nullable();
            $table->string('app_code',100)->nullable();
            $table->date('pay_date')->nullable();
            $table->string('v_code',255)->nullable();
            $table->string('s_key',255)->nullable();
            $table->text('url_request')->nullable();
            $table->text('callback')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_transactions_id')->references('id')->on('client_transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('molpay_transactions');
    }
}
