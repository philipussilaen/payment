<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOpenFixedUrlToCompanyAccessTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_access_tokens', function (Blueprint $table) {
            $table->string('open_payment_callback',255)->nullable()->after('token');
            $table->string('fixed_payment_callback',255)->nullable()->after('open_payment_callback');
            $table->text('additional_parameter_callback')->nullable()->after('fixed_payment_callback');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_access_tokens', function (Blueprint $table) {
            $table->dropColumn('open_payment_callback');
            $table->dropColumn('fixed_payment_callback');
            $table->dropColumn('additional_parameter_callback');
        });
    }
}
