<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSubPaymentIdToTransactionDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_transaction_details', function (Blueprint $table) {
            $table->string('sub_payment_id',255)->nullable()->after('client_transactions_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_transaction_details', function (Blueprint $table) {
            $table->dropColumn('sub_payment_id');
        });
    }
}
