<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPaymentChannelsTableDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_channels', function (Blueprint $table) {
            $table->string('type',32)->after('payment_vendors_id')->nullable();
            $table->string('text',256)->after('name')->nullable();
            $table->string('description',512)->after('text')->nullable();
            $table->string('image_url',256)->after('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_channels', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('text');
            $table->dropColumn('description');
            $table->dropColumn('image_url');
        });
    }
}
