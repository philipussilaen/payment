<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMolpayChannelLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('molpay_channel_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',100)->nullable();
            $table->string('name',150)->nullable();
            $table->string('filename',150)->nullable();
            $table->float('range_amount')->nullable();
            $table->string('currency',50)->nullable()->default('MYR');
            $table->string('downtime',150)->nullable();
            $table->text('information')->nullable();
            $table->string('status',10)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        \Illuminate\Support\Facades\Artisan::call('db:seed',array('--class'=>'MolPayChannelSeeder'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('molpay_channel_lists');
    }
}
