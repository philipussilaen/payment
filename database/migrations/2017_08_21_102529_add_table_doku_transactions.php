<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableDokuTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doku_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_transactions_id');
            $table->string('method_code',50)->nullable();
            $table->string('payment_channel',5)->nullable();
            $table->float('amount')->nullable();
            $table->string('channel_prefix',25)->nullable();
            $table->string('payment_code',100)->nullable();
            $table->string('virtual_number',125)->nullable();
            $table->string('mcn',100)->nullable();
            $table->string('words',255)->nullable();
            $table->string('result',100)->nullable();
            $table->string('bank',255)->nullable();
            $table->string('status_type',10)->nullable();
            $table->string('approval_code',50)->nullable();
            $table->string('response_code',20)->nullable();
            $table->string('customer_name',150)->nullable();
            $table->string('brand',50)->nullable();
            $table->string('session_id',100)->nullable();
            $table->dateTime('payment_date')->nullable();
            $table->timestamps();

            $table->foreign('client_transactions_id')->references('id')->on('client_transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doku_transactions');
    }
}
