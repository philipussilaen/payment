<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmoneyCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emoney_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_transactions_id');
            $table->string('bank',128)->nullable();
            $table->string('card_number',128)->nullable();
            $table->string('reader_id',512)->nullable();
            $table->string('merchant_id',512)->nullable();
            $table->decimal('transaction_amount',12,2);
            $table->decimal('paid_amount',12,2)->nullable();
            $table->decimal('last_balance',12,2)->nullable();
            $table->string('status',32);
            $table->timestamps();

            $table->foreign('client_transactions_id')->references('id')->on('client_transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emoney_cards');
    }
}
