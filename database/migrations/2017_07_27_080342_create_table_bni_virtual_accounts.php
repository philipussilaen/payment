<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBniVirtualAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bni_virtual_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_transactions_id');
            $table->string('billing_type',25);
            $table->string('virtual_account_number',45);
            $table->dateTimeTz('datetime_created');
            $table->dateTimeTz('datetime_expired');
            $table->dateTimeTz('datetime_payment')->nullable();
            $table->bigInteger('payment_amount')->nullable();
            $table->string('payment_ntb',20)->nullable();
            $table->string('va_status',20);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_transactions_id')->references('id')->on('client_transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bni_virtual_accounts');
    }
}
