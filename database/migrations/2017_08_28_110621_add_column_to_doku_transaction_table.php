<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToDokuTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doku_transactions', function (Blueprint $table) {
            $table->integer('payment_amount')->nullable()->after('amount');
            $table->integer('last_payment_amount')->nullable()->after('amount');

            $table->renameColumn('amount','transaction_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doku_transactions', function (Blueprint $table) {
            $table->dropColumn(['payment_amount','last_payment_amount']);
        });
    }
}
