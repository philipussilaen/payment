<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentTypeToPaymentChannelChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_channel_charges', function (Blueprint $table) {
            $table->string('payment_type',16)->after('name')->default('fixed')->comment('Payment Type, Open or Fixed');
            $table->string('charges_type',16)->after('payment_type')->default('subtract')->comment('Charges Type, Subtract: Total paid will be deducted, Adders: Total must paid will be added ');
            $table->text('rules')->after('type')->nullable()->comment('json Value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_channel_charges', function (Blueprint $table) {
            $table->dropColumn('payment_type');
            $table->dropColumn('charges_type');
            $table->dropColumn('rules');
        });
    }
}
