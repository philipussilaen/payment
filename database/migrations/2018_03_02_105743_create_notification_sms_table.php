<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_sms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('module',256);
            $table->unsignedInteger('client_transaction_id',false)->nullable();
            $table->string('vendor',128);
            $table->string('to',32);
            $table->text('sms');
            $table->smallInteger('status',false,false);
            $table->timestamps();

            $table->foreign('client_transaction_id')->references('id')->on('client_transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_sms');
    }
}
