<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClientTransactionDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_transaction_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_transactions_id');
            $table->string('description',150);
            $table->bigInteger('amount');
            $table->string('status',45)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_transactions_id')->references('id')->on('client_transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_transaction_details');
    }
}
