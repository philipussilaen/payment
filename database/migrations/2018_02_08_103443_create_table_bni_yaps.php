<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBniYaps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bni_yaps', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_transactions_id');
            $table->integer('transaction_amount',false,false);
            $table->integer('paid_amount',false,false)->nullable();
            $table->integer('payment_amount_fee',false,false)->nullable();
            $table->text('qr_data')->nullable();
            $table->string('app_accounts_payment_id',256)->nullable();
            $table->string('mvisa_transaction_id',256)->nullable();
            $table->string('account_id',256)->nullable();
            $table->string('merchant_pan',32)->nullable();
            $table->string('merchant_pan_raw',32)->nullable();
            $table->string('merchant_name',256)->nullable();
            $table->dateTime('transaction_date')->nullable();
            $table->string('consumer_name',256)->nullable();
            $table->string('consumer_pan',256)->nullable();
            $table->string('consumer_pan_raw',256)->nullable();
            $table->string('approval_code',32)->nullable();
            $table->string('status',32);
            $table->timestamps();

            $table->foreign('client_transactions_id')->references('id')->on('client_transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bni_yaps');
    }
}
