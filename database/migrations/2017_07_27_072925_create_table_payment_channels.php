<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentChannels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_channels', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('payment_vendors_id');
            $table->string('name',100);
            $table->string('code',45)->nullable();
            $table->string('status',45)->default('NOT_ACTIVE');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('payment_vendors_id')->references('id')->on('payment_vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_channels');
    }
}
