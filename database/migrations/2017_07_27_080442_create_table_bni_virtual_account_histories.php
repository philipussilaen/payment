<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBniVirtualAccountHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bni_virtual_account_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bni_virtual_accounts_id');
            $table->bigInteger('amount');
            $table->string('customer_name',100);
            $table->dateTimeTz('datetime_expired');
            $table->string('description',255);
            $table->bigInteger('payment_amount');
            $table->bigInteger('cumulative_payment_amount')->nullable();
            $table->string('payment_ntb',45)->nullable();
            $table->dateTimeTz('datetime_payment')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('bni_virtual_accounts_id')->references('id')->on('bni_virtual_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bni_virtual_account_histories');
    }
}
