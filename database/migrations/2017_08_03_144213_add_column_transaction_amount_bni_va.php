<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTransactionAmountBniVa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bni_virtual_accounts', function (Blueprint $table) {
            $table->bigInteger('transaction_amount')->after('datetime_payment')->default(0);
            $table->bigInteger('last_payment_amount')->after('payment_amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bni_virtual_accounts', function (Blueprint $table) {
            $table->dropColumn('transaction_amount');
            $table->dropColumn('last_payment_amount');
        });
    }
}
