<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminFeePaidAmountToClientTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_transaction_details', function (Blueprint $table) {
            $table->decimal('amount',15,2)->change();
            $table->decimal('paid_amount',15,2)->after('amount')->nullable();
            $table->decimal('channel_admin_fee',8,2)->after('paid_amount')->nullable();
            $table->decimal('customer_admin_fee',8,2)->after('channel_admin_fee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_transaction_details', function (Blueprint $table) {
            $table->integer('amount')->change();
            $table->dropColumn('paid_amount');
            $table->dropColumn('channel_admin_fee');
            $table->dropColumn('customer_admin_fee');
        });
    }
}
