<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBniVirtualPrefixToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bni_virtual_prefix_token', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_access_tokens_id');
            $table->string('prefix',2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('company_access_tokens_id')->references('id')->on('company_access_tokens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bni_virtual_prefix_token');
    }
}
